-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 15 Jun 2021 pada 12.31
-- Versi server: 10.4.18-MariaDB
-- Versi PHP: 7.4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ci_pak`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `guru`
--

CREATE TABLE `guru` (
  `id` int(11) NOT NULL,
  `nip` varchar(255) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `no_karpeg` varchar(255) NOT NULL,
  `jenis_kelamin` varchar(255) NOT NULL,
  `tmp_lahir` varchar(255) NOT NULL,
  `tgl_lahir` varchar(255) NOT NULL,
  `pendidikan_terakhir` varchar(255) NOT NULL,
  `pangkat` varchar(255) NOT NULL,
  `nrg` varchar(255) NOT NULL,
  `nuptk` varchar(255) NOT NULL,
  `mapel` varchar(255) NOT NULL,
  `jam_mengajar` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `guru`
--

INSERT INTO `guru` (`id`, `nip`, `nama`, `no_karpeg`, `jenis_kelamin`, `tmp_lahir`, `tgl_lahir`, `pendidikan_terakhir`, `pangkat`, `nrg`, `nuptk`, `mapel`, `jam_mengajar`) VALUES
(5, '', 'laila', '1234', 'P', 'jombang', '2021-06-12', 'sma', '1', '456', '789', '', '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `komponen`
--

CREATE TABLE `komponen` (
  `id` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `type` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `komponen`
--

INSERT INTO `komponen` (`id`, `nama`, `type`) VALUES
(1, 'Perilaku Guru Sehari-hari', 1),
(2, 'Hubungan Guru Dengan Teman Sejawat', 1),
(3, 'Perilaku Profesional Guru', 1),
(4, 'Mengenal Karakteristik peserta didik', 2),
(5, 'Menguasi teori belajar dan prinsip-prinsip pembelajaran', 2),
(7, 'Pengembangan Kurikulum', 2),
(8, 'Memahami dan mengembangkan potensi', 2),
(9, 'Komunikasi dengan peserta didik', 2),
(10, 'Penilaian dan Evaluasi', 2),
(11, 'Bertindak sesuai dengan norma agama, hukum, sosial dan kebudayaan nasional Indonesia', 2),
(12, 'Menunjukkan pribadi yang dewasa dan teladan', 2),
(13, 'Etos kerja, tanggung jawab yang tinggi dan rasa bangga menjadi guru', 2),
(14, 'Bersifat iklusif, bertindak objektif serta tidak deskriminatif', 2),
(15, 'Komunikasi dengan sesama guru, tenaga pendidikan, orang tua peserta didik, dan masyarakat', 2),
(16, 'Penguasaan materi struktur konsep dan pola pikir keilmuan yang mendukung mata pelajaran yang di ampu', 2),
(17, 'Mengembangkan keprofesian melalu tindakan reflektif', 2),
(18, 'Kegiatan pembelajaran yang mendidik', 2);

-- --------------------------------------------------------

--
-- Struktur dari tabel `pegawai`
--

CREATE TABLE `pegawai` (
  `id` int(11) NOT NULL,
  `nip` varchar(255) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `no_karpeg` varchar(255) NOT NULL,
  `jenis_kelamin` varchar(255) NOT NULL,
  `tmp_lahir` varchar(255) NOT NULL,
  `tgl_lahir` varchar(255) NOT NULL,
  `pendidikan_terakhir` varchar(255) NOT NULL,
  `pangkat` varchar(255) NOT NULL,
  `nrg` varchar(255) NOT NULL,
  `nuptk` varchar(255) NOT NULL,
  `mapel` varchar(255) NOT NULL,
  `jam_mengajar` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `pegawai`
--

INSERT INTO `pegawai` (`id`, `nip`, `nama`, `no_karpeg`, `jenis_kelamin`, `tmp_lahir`, `tgl_lahir`, `pendidikan_terakhir`, `pangkat`, `nrg`, `nuptk`, `mapel`, `jam_mengajar`) VALUES
(1, '', 'admin', '', '', '', '', '', '', '', '', '', ''),
(2, 'Omnis aliquam assume', 'Dolor architecto qui', 'Quaerat mollitia min', 'L', 'Quasi laboris odit m', '1970-05-21', 'Veniam aute qui ali', 'Itaque quas ex ipsa', 'Aspernatur rerum vol', 'Et a quasi tempora d', 'Et eos accusamus tot', 'Laboris aute volupta');

-- --------------------------------------------------------

--
-- Struktur dari tabel `penilaian`
--

CREATE TABLE `penilaian` (
  `id` int(11) NOT NULL,
  `id_penilai` int(11) DEFAULT NULL,
  `id_pegawai` int(11) DEFAULT NULL,
  `id_dinilai` int(11) NOT NULL,
  `id_pernyataan` int(11) NOT NULL,
  `nilai` varchar(255) NOT NULL,
  `tgl` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `penilaian`
--

INSERT INTO `penilaian` (`id`, `id_penilai`, `id_pegawai`, `id_dinilai`, `id_pernyataan`, `nilai`, `tgl`) VALUES
(170, NULL, 2, 5, 38, '70', 1623640429),
(171, NULL, 2, 5, 39, '100', 1623640429),
(172, NULL, 2, 5, 40, '70', 1623640429),
(173, NULL, 2, 5, 41, '100', 1623640429),
(174, NULL, 2, 5, 42, '70', 1623640429),
(175, NULL, 2, 5, 43, '70', 1623640429),
(176, NULL, 2, 5, 44, '70', 1623640429),
(177, NULL, 2, 5, 45, '40', 1623640429),
(178, NULL, 2, 5, 46, '70', 1623640429),
(179, NULL, 2, 5, 47, '40', 1623640429),
(180, NULL, 2, 5, 48, '70', 1623640429),
(181, NULL, 2, 5, 49, '100', 1623640429),
(182, NULL, 2, 5, 50, '100', 1623640429),
(183, NULL, 2, 5, 51, '70', 1623640429),
(184, NULL, 2, 5, 52, '70', 1623640429),
(185, NULL, 2, 5, 53, '40', 1623640429),
(186, NULL, 2, 5, 54, '40', 1623640429),
(187, NULL, 2, 5, 55, '40', 1623640429),
(188, NULL, 2, 5, 56, '100', 1623640429),
(189, NULL, 2, 5, 57, '70', 1623640429),
(190, NULL, 2, 5, 58, '40', 1623640429),
(191, NULL, 2, 5, 59, '70', 1623640429),
(192, NULL, 2, 5, 60, '40', 1623640429),
(193, NULL, 2, 5, 61, '70', 1623640429),
(194, NULL, 2, 5, 62, '70', 1623640429),
(195, NULL, 2, 5, 63, '100', 1623640429),
(196, NULL, 2, 5, 64, '70', 1623640429),
(197, NULL, 2, 5, 65, '40', 1623640429),
(198, NULL, 2, 5, 66, '100', 1623640429),
(199, NULL, 2, 5, 67, '70', 1623640429),
(200, NULL, 2, 5, 68, '100', 1623640429),
(201, NULL, 2, 5, 69, '40', 1623640429),
(202, NULL, 2, 5, 70, '40', 1623640429),
(203, NULL, 2, 5, 71, '100', 1623640429),
(204, NULL, 2, 5, 72, '70', 1623640429),
(205, NULL, 2, 5, 73, '40', 1623640429),
(206, NULL, 2, 5, 74, '40', 1623640429),
(207, NULL, 2, 5, 75, '100', 1623640429),
(208, NULL, 2, 5, 76, '40', 1623640429),
(209, NULL, 2, 5, 77, '40', 1623640429),
(210, NULL, 2, 5, 78, '70', 1623640429),
(211, NULL, 2, 5, 79, '70', 1623640429),
(212, NULL, 2, 5, 80, '40', 1623640429),
(213, NULL, 2, 5, 81, '40', 1623640429),
(214, NULL, 2, 5, 82, '100', 1623640429),
(215, NULL, 2, 5, 83, '100', 1623640429),
(216, NULL, 2, 5, 84, '100', 1623640429),
(217, NULL, 2, 5, 85, '40', 1623640429),
(218, NULL, 2, 5, 86, '100', 1623640429),
(219, NULL, 2, 5, 87, '40', 1623640429),
(220, NULL, 2, 5, 88, '40', 1623640429),
(221, NULL, 2, 5, 89, '40', 1623640429),
(222, NULL, 2, 5, 90, '70', 1623640429),
(223, NULL, 2, 5, 91, '70', 1623640429),
(224, NULL, 2, 5, 92, '40', 1623640429),
(225, NULL, 2, 5, 93, '100', 1623640429),
(226, NULL, 2, 5, 94, '100', 1623640429),
(227, NULL, 2, 5, 95, '40', 1623640429),
(228, NULL, 2, 5, 96, '70', 1623640429),
(229, NULL, 2, 5, 97, '40', 1623640429),
(230, NULL, 2, 5, 98, '100', 1623640429),
(231, NULL, 2, 5, 99, '70', 1623640429),
(232, NULL, 2, 5, 100, '70', 1623640429),
(233, NULL, 2, 5, 101, '70', 1623640429),
(234, NULL, 2, 5, 102, '40', 1623640429),
(235, NULL, 2, 5, 103, '40', 1623640429),
(236, NULL, 2, 5, 104, '70', 1623640429),
(237, NULL, 2, 5, 105, '40', 1623640429),
(238, NULL, 2, 5, 106, '100', 1623640429),
(239, NULL, 2, 5, 107, '100', 1623640429),
(240, NULL, 2, 5, 108, '100', 1623640429),
(241, NULL, 2, 5, 109, '70', 1623640429),
(242, NULL, 2, 5, 110, '40', 1623640429),
(243, NULL, 2, 5, 111, '70', 1623640429),
(244, NULL, 2, 5, 112, '40', 1623640429),
(245, NULL, 2, 5, 113, '70', 1623640429),
(246, NULL, 2, 5, 114, '70', 1623640429),
(247, NULL, 2, 5, 115, '40', 1623640429),
(248, NULL, 2, 5, 116, '100', 1623640429);

-- --------------------------------------------------------

--
-- Struktur dari tabel `pernyataan`
--

CREATE TABLE `pernyataan` (
  `id` int(11) NOT NULL,
  `id_komponen` int(11) NOT NULL,
  `pernyataan` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `pernyataan`
--

INSERT INTO `pernyataan` (`id`, `id_komponen`, `pernyataan`) VALUES
(8, 1, 'Guru mentaati peraturan yang berlaku di sekolah.'),
(9, 1, 'Guru bekerja sesuai jadwal yang ditetapkan.'),
(10, 1, 'Guru berpakaian rapi dan/atau sopan.'),
(11, 1, 'Guru rajin mengikuti upacara bendera.'),
(12, 1, 'Guru berperilaku baik terhadap saya dan guru lain.'),
(13, 1, 'Guru bersedia menerima kritik dan saran dari saya atau guru lain.'),
(14, 1, 'Guru dapat menjadi teladan bagi saya dan teman-teman.'),
(15, 1, 'Guru pandai mengendalikan diri.'),
(16, 1, 'Guru ikut aktif menjaga lingkungan sekolah bebas dari asap rokok.'),
(17, 1, 'Guru berpartisipasi aktif dalam kegiatan ekstrakulikuler.'),
(18, 1, 'Guru berpartisipasi aktif dalam kegiatan peningkatan prestasi sekolah.'),
(19, 2, 'Guru bersikap ramah kepada saya atau orang lain.'),
(20, 2, 'Guru berbahasa santun kepada saya atau orang lain.'),
(21, 2, 'Guru memberi motivasi kepada saya atau teman-teman guru lain.'),
(22, 2, 'Guru pandai berkomunikasi secara lisan atau tertulis.'),
(23, 2, 'Guru memotivasi diri dan rekan sejawat secara aktif dan kreatif dalam melaksanakan proses pendidikan.'),
(24, 2, 'Guru menciptakan suasana kekeluargaan di dalam dan luar sekolah.'),
(25, 2, 'Guru mudah bekerjasama dengan saya atau guru lainnya'),
(26, 2, 'Guru bersedia diajak berdiskusi tentang segala hal terkait kepentingan peserta didik dan sekolah.'),
(27, 2, 'Guru bersedia membantu menyelesaikan masalah saya dan guru lainnya.'),
(28, 2, 'Guru menghargai kemampuan saya dan guru lainnya.'),
(29, 3, 'Guru memiliki kreativitas dalam pembelajaran.'),
(30, 3, 'Guru memiliki pengetahuan dan keterampilan Teknologi informasi (TI) yang memadai.'),
(31, 3, 'Guru memiliki perangkat pembelajaran yang lengkap.'),
(32, 3, 'Guru ada di sekolah meskipun tidak mengajar di kelas.'),
(33, 3, 'Guru memulai pembelajaran tepat waktu.'),
(34, 3, 'Guru mengakhiri pembelajaran tepat waktu.'),
(35, 3, 'Guru memberikan tugas kepada peserta didik apabila berhalangan hadir untuk mengajar.'),
(36, 3, 'Guru memberi informasi kepada saya atau guru lain jika berhalangan hadir untuk mengajar.'),
(37, 3, 'Guru memperlakukan peserta didik dengan penuh kasih sayang.'),
(38, 4, 'Guru dapat mengidentifikasi karakteristik belajar setiap peserta didik di kelasnya.'),
(39, 4, 'Guru memastikan bahwa semua peserta didik mendapatkan kesempatan yang sama untuk berpartisipasi aktif dalam kegiatan pembelajaran.'),
(40, 4, 'Guru dapat mengatur kelas untuk memberikan kesempatan belajar yang sama pada semua peserta didik dengan kelainan fisik dan kemampuan  belajar yang berbedaa.'),
(41, 4, 'Guru mencoba mengetahui penyebab penyimpangan perilaku pserta didik untuk mencegah agar perilaku tersebut tidak merugikan peserta didik lainnya.'),
(42, 4, 'Guru membantu mengembangkan potensi dan mengatasi kekurangan peserta didik.'),
(43, 4, 'Guru memperhatikan peserta didik dengan kelemahan fisik tertentu agar dapat mengikuti aktivitas pembelajaran,sehingga peserta didik tersebut tidak termaginalkan (tersisihkan,diolok-olok,minder,dsb)'),
(44, 5, 'Guru memberi kesempatan kepada peserta didik untuk menguasi materi pembelajaran sesuai usia dan kemampuan belajarnya  melalui pengaturan proses pembelajaran dan aktivitas yang bervariasi.'),
(45, 5, 'Guru selalu memastikan tingkat pemahaman peserta didik terhadap materi pembelajaran tertentu dan menyesuaikan aktivitas pembelajaran berikutnya berdasarkan tingkat pemahaman tersebut.'),
(46, 5, 'Guru dapat menjelaskan alasan pelaksanaan kegiatan / aktivitas yang di lakukannya , baik yang sesuai maupun yang berbeda dengan rencana,terkait keberhasilan pembelajaran.'),
(47, 5, 'Guru menggunakan berbagai teknik untuk memotivasi kemauan belajar peserta didik.'),
(48, 5, 'Guru merencankan kegiatan pembelajaran yang saling terkait satu sama lain dengan memperhatikan tujuan pembelajaran maupun proses belajar peserta didik.'),
(49, 5, 'Guru memperhatikan respon peserta didik yang belum/kurang memahami materi pembelajaran yang di ajarkan dan menggunakannya untuk memperbaiki rancangan pembelajaran berikutnya'),
(50, 7, 'Guru dapat menyusun silabus yang sesuai dengan kurikulum'),
(51, 7, 'Guru merancang rencana pembelajarann yang sesuai dengan silabus untuk membahas materi ajar tertentu agar peserta didik dapat mencapai kompetensi dasar yang di tetapkan.'),
(52, 7, 'Guru mengikuti urutan materi pembelajaran dengan memperhatikan tujuan pembelajaran.'),
(53, 7, 'Guru memilih materi pembelajaran yang sesuai dengan tujuan pembelajaran,tepat dan mutakhir,sesuai dengan usia tingkat kemampuan belajar peserta didik daan dapat dilaksanakan kelas sesuai dengan konteks kehidupan sehari-hari peserta didik.'),
(54, 18, 'Guru melaksanakan aktivitas pembelajaran sesuai dengan rancangan yang telah di susun secara lengkap dan pelaksanaan aktivitas tersebut mengindikasikan bahwa guru mengerti tentang tujuannya'),
(55, 18, 'Guru melaksanakan aktivitas pembelajaran yang bertujuan untuk membantu proses belajar peserta didik, bukan untukmenguji sehingga membuat peserta didik merasa tertekan.'),
(56, 18, 'Guru mengomunikasikan informasi baru (misalnya materi tambahan ) sesuaai dengan usia dan tingkat kemampuan belajar peserta didik.'),
(57, 18, 'Guru menyikapi kesalahan yang dilakukan peserta didik sebagai tahapan proses pembelajaran,bukan semata-mata kesalahan yang harus dikoreksi.'),
(58, 18, 'Guru melaksanakan kegiatan pembelajaran sesuai kurikulum dan mengkaitkan dengan konteks kehidupan sehari-hari peserta didik.'),
(59, 18, 'Guru melakukan aktivitas pembelajaran secara bervariasi dengan waktu yang cukup untuk kegiatan pembelajaran yang sesuai dengan usia tingkat kemampuan belajara dan mempertahankan perhatian peserta didik.'),
(60, 18, 'Guru mengola kelas dengan efektif tanpa mendominasi atau sibuk dengan kegiatannya sendiri  agar semua waktu peserta dapat termanfaatkan secra produktif.'),
(61, 18, 'Guru mampu menyesuaikan aktivitas pembelajaran yang di rancang dengan kondisi kelas.'),
(62, 18, 'Guru memberikan banyak kesempatan kepada peserta didik untuk bertanya,mempraktekkan dan berinteraksi dengan peserta didik lain.'),
(63, 18, 'Guru mengatur pelaksanaan aktivitas pembelajaran  secara sistematis untuk membantu proses belajar peserta didik.'),
(64, 18, 'Guru menggunakan alat bantu mengajar,dan/video-visual (termasuk TIK) untuk meningkatkan motivasi belajar peserta didik dalam mencapai tujuan pembelajaran.'),
(65, 8, 'Guru menganalisis hasil belajar berdasarkan segala bentuk penilian terhadap setiap peserta didik untuk mengetahui tingkat kemajuan masing-masing.'),
(66, 8, 'Guru merancang dan melaksanakan aktivitas pembelajaran yang mendorong peserta didik untuk belajar sesuai dengan kecakapan dan pola belajar masing-masing.'),
(67, 8, 'Guru dan melaksanakan aktivitas pembelajaran untuk memunculkan daya kreativitas dan kemampuan berfikir kritis peserta didik'),
(68, 8, 'Guru secara aktif membantu peserta didik dalam proses pembelajaran dengan memberikan perhatian kepada setiap individu.'),
(69, 8, 'Guru dapat mengidentifikasi dengan benar tentang bakat,minat,potensi dan kesulitan belajar masing-masing peserta didik.'),
(70, 8, 'Guru memberikan kesempatan belajar kepada peserta didik sesuai dengan cara belajarnya masing-masing.'),
(71, 8, 'Guru memusatkan perhatian interaksi dengan peserta didik dan mendorong untuk memahami dan menggunakan informasi yang disampaikan.'),
(72, 9, 'Guru menggunkan pertanyaan untuk mengetahui pemahaman dan menjaga partisipasi peserta didik,termasuk memberikan pertanyaan terbuka yang menuntut peserta didik untuk menjawab dengan ide dan pengetahuan mereka.'),
(73, 9, 'Guru memberikan perhatian dan mendengarkan semua pertanyaan dan tanggapan peserta didik, tanpa menginterupsi, kecuali jika di perlukan untuk membantu atau mengklarifikasi pertanyaan/tanggapan tersebut.'),
(74, 9, 'Guru menanggapi pertanyaan peserta didik secara tepat,benar, dan mutakhir , sesuai tujuan pembelajaran da nisi kurikulum, tanpa mempermalukannya.'),
(75, 9, 'Guru menyajikan kegiatan pembelajaran yang dapat menumbuhkan kerja sama yang baik antar peserta didik.'),
(76, 9, 'Guru mendengarkan dan memberikan perhatian terhadap semua jawaban peserta didik baik yang benar maupun yang di anggap salah untuk mengukur tingkat pemahaman peserta didik.'),
(77, 9, 'Guru mendengarkan dan memberikan perhatian terhadap semua jawaban peserta didik baik yang benar maupun yang di anggap salah untuk mengukur tingkat pemahaman peserta didik.'),
(78, 9, 'Guru memberikan perhatian terhadap pertanyaan peserta didik dan meresponnya secara lengkap dan relevenuntuk menghilangkan kebingungan kepada peserta didik.'),
(79, 10, 'Guru menyusun alat penilaian sesuai dengan tujuan pembelajaran mencapai kompetensi tertentu seperti ang tertulis di dalam RPP.'),
(80, 10, 'Guru melaksanakan penilaian dengan berbagai teknik dan jenis penilaian, selain penilaian formal yang di laksanakan sekolah, dan mengumumkan hasil serta implimasikanya kepada peserta didik tentan tingkat pemahaman terhadap materi pembelajaran yang telah da'),
(81, 10, 'Guru menganilisis hasil penilaian untuk mengidentifikasikan topik/kompetensi dasar yang sulit sehingga diketahui kekuatan dan kelemahan masing-masing peserta didik untuk keperluan remdial dan pengayaan.'),
(82, 10, 'Guru memanfaatkan masukan dari peserta didik dan merefleksikannya untuk meningkatkan pmbelajaran selanjutnya, dan dapat membuktikannya melalui catatan,jurnal pembelajaran,rancangan pembelajaran,materi tambahan, dan sebagainya.'),
(83, 10, 'Guru memanfaatkan hasil penilaian sebagai bahan penyusun rancangan pembelajaran yang akan di lakukan  selanjutnya.'),
(84, 11, 'Guru menghargai dan mempromosikan prinsip-prinsip Pancasila sebagai dasar ideology dan etika bagi semua warga Indonesia.'),
(85, 11, 'Guru mengembangkan kerjasama dan membina kebersamaan dengan teman sejawat tanpa memperhatikan perbedaan yang ada.'),
(86, 11, 'Guru saling menghormati dan menghargai teman sejawat sesuai dengan kondisi dan keberadaan masing-masing.'),
(87, 11, 'Guru memiliki rasa persatuan dan kesatuan sebagai bangsa Indonesia.'),
(88, 11, 'Guru mempunyai pandangan yang luas tentang keberagamaan bangsa Indonesia.'),
(89, 12, 'Guru bertingkah laku sopan dalam berbicara,berpenampilan, dan berbuat terhadap semua peserta didik,orang tua dan teman sejawat.'),
(90, 12, 'Guru mau membagi pengalamannya dengan teman sejawat,termasuk mengundang mereka untuk mengobservasi cara mengajarnya dan memberikan masukan.'),
(91, 12, 'Guru mampu mengelola pembelajaran yang membuktikan bahwa guru dihormati oleh peserta didik , sehingga semua peserta didik selalu memperhatikan guru dan berpartisipasi aktif dalam proses pembelajaran.'),
(92, 12, 'Guru bersikap dewasa dalam menerima masukan dari peserta didik dan memberikan kesempatan kepada peserta didik untuk berpartisipasi dalam proses pembelajaran.'),
(93, 12, 'Guru berperilaku baik untuk mencitrakan nama baik sekolah.'),
(94, 13, 'Guru mengawali dan mengakhiri pembelajaran dengan tepat waktu.'),
(95, 13, 'Jika guru harus meninggalkan kelas, guru mengaktifkan siswa dengan melakukan hal-hal produktif terkait dengan mata pelajaran, dan meminta guru piket atau guru lain untuk mengwasi kelas.'),
(96, 13, 'Guru memenuhi jam mengajar dan dapat melakukan semua kegiatan lain luar jam mengajar berdasarkan ijin dan persetujuan pengelola sekolah.'),
(97, 13, 'Guru meminta ijin dan memberitahu lebih awal dengan memberikan alasan dan bukti sah jika tidak menghadiri kegiatan yang telah di rencanakan, termasuk proses pembelajaran dikelas.'),
(98, 13, 'Guru menyelesaikan semua tugas adminitratif dan non-pembelajaran dengan tepat waktu sesuai standar yang ditetapkan.'),
(99, 13, 'Guru memanfaatkan waktu luang selain mengajar untuk kegiatan yang produktif terkait dengan tugasnya.'),
(100, 13, 'Guru memberikan konstribusi terhadap pengembangan sekolah dan mempunyai prestasi yang berdampak positif terhadap nama baik sekolah.'),
(101, 13, 'Guru merasa bangga dengan profesinya sebagai guru.'),
(102, 14, 'Guru memperlakukan semua peserta didik secara adil.memberikan perhatian dan bantuan sesuai kebutuhan masing masing,tanpa memperdulikan faktor personal.'),
(103, 14, 'Guru menjaga hubungan baik dan peduli dengan teman sejawat (bersifat inklusif),serta berkontribusi positif terhadap semua diskusi formal dan informal terkait dengan pekerjaannya.'),
(104, 14, 'Guru sering berinteraksi dengan peserta didik dan tidak membatasi perhatiannya hanya pada Kelompok tertentu (misalnya peserta didik yang pandai,kaya, berasal dari daerah yang sama dengan guru)'),
(105, 15, 'Guru menyampaikan tentang informasi kemajuan, kesulitan,dan potensi peserta didik kepada orang tuanya baik dalam pertemuan formal maupun tidak formal antara guru dengan orang tua,teman sejawat dan dapat menunjukkan buktinya.'),
(106, 15, 'Guru ikut berperan aktif dalam kegiatan diluar pembelajaran yang diselenggarakan oleh sekolah dan masyarakat dan dapat memberikan bukti keikutsertaan nya.'),
(107, 15, 'Guru memperhatikan sekolah sebagai bagian dari masyarakat, berkomunikasi dengan masyarakat sekitar,serta berperan dalam kegiatan sosial di masyarakat.'),
(108, 16, 'Guru melakukan pemetaan standar kompetensi dan kompetensi dasar untuk mata pelajaran yang di ampu nya,untuk mengidentifikasi materi pembelajaran yang dianggap sulit, melakukan perencanaan dan pelaksanaan pembelajaran,dan memperkirakan alokasi waktu yang d'),
(109, 16, 'Guru menyertakan informasi yang tepat dan mutakhir didalam perencanaan dan pelaksanaan pembelajaran.'),
(110, 16, 'Guru menyusun materi perencanaan dan pelaksanaan pembelajaran yang berisi informasi yang tepat, mutakhir,dan yang membantu peserta didik untuk memahami Konsep materi pembelajaran.'),
(111, 17, 'Guru melakukan evaluasi diri secara spesifik,lengkap,dan didukung dengan contoh pengalaman diri sendiri.'),
(112, 17, 'Guru memiliki jurnal pembelajaran,catatan masukan dari kolega,atau hasil penilaian proses pembelajaran sebagai bukti yang menggambarkan kinerjanya.'),
(113, 17, 'Guru memanfaatkan bukti gambaran kinerjanya untuk mengembangkan perencanaan dan pelaksanaan pembelajaran selanjutnya dalam program pengembangan keprofesian berkelanjutan (PKB)'),
(114, 17, 'Guru dapat mengaplikasikan pengalaman PKB dalam perencanaan, pelaksanaan, penilaian pembelajaran dan tindak lanjutnya.'),
(115, 17, 'Guru melakukan penelitian, mengembangkan karya inovasi, mengikuti kegiatan ilmiah,(misalnya seminar, konferensi) dan aktif dalam melaksanakan PKB.'),
(116, 17, 'Guru dapat memanfaatkan TIK dalam berkomunikasi dan pelaksanaan PKB.');

-- --------------------------------------------------------

--
-- Struktur dari tabel `role`
--

CREATE TABLE `role` (
  `id` int(11) NOT NULL,
  `nama` varchar(126) NOT NULL,
  `link` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `role`
--

INSERT INTO `role` (`id`, `nama`, `link`) VALUES
(1, 'Admin', 'admin'),
(2, 'Guru', 'guru'),
(3, 'Kepala Sekolah', 'kepsek');

-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `id_guru` int(50) DEFAULT NULL,
  `id_pegawai` int(11) DEFAULT NULL,
  `username` varchar(126) NOT NULL,
  `password` varchar(256) NOT NULL,
  `role_id` int(2) NOT NULL,
  `is_active` int(1) NOT NULL,
  `gambar` varchar(256) NOT NULL,
  `date_created` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `user`
--

INSERT INTO `user` (`id`, `id_guru`, `id_pegawai`, `username`, `password`, `role_id`, `is_active`, `gambar`, `date_created`) VALUES
(1, NULL, 1, 'admin', '21232f297a57a5a743894a0e4a801fc3', 1, 1, 'admin1.png', 1616218670),
(9, NULL, 2, 'kepsek', '21232f297a57a5a743894a0e4a801fc3', 3, 1, 'kepsek.png', 1616218670),
(11, 5, NULL, 'laila', 'f30618ed64655812746272636a992b95', 2, 1, 'avatar.jpg', 1623477759);

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `guru`
--
ALTER TABLE `guru`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `komponen`
--
ALTER TABLE `komponen`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `pegawai`
--
ALTER TABLE `pegawai`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `penilaian`
--
ALTER TABLE `penilaian`
  ADD PRIMARY KEY (`id`),
  ADD KEY `dinilai` (`id_dinilai`),
  ADD KEY `pernyataan` (`id_pernyataan`);

--
-- Indeks untuk tabel `pernyataan`
--
ALTER TABLE `pernyataan`
  ADD PRIMARY KEY (`id`),
  ADD KEY `komponen` (`id_komponen`);

--
-- Indeks untuk tabel `role`
--
ALTER TABLE `role`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `guru`
--
ALTER TABLE `guru`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT untuk tabel `komponen`
--
ALTER TABLE `komponen`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT untuk tabel `pegawai`
--
ALTER TABLE `pegawai`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `penilaian`
--
ALTER TABLE `penilaian`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=328;

--
-- AUTO_INCREMENT untuk tabel `pernyataan`
--
ALTER TABLE `pernyataan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=117;

--
-- AUTO_INCREMENT untuk tabel `role`
--
ALTER TABLE `role`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT untuk tabel `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `penilaian`
--
ALTER TABLE `penilaian`
  ADD CONSTRAINT `dinilai` FOREIGN KEY (`id_dinilai`) REFERENCES `guru` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `pernyataan` FOREIGN KEY (`id_pernyataan`) REFERENCES `pernyataan` (`id`) ON DELETE CASCADE;

--
-- Ketidakleluasaan untuk tabel `pernyataan`
--
ALTER TABLE `pernyataan`
  ADD CONSTRAINT `komponen` FOREIGN KEY (`id_komponen`) REFERENCES `komponen` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
