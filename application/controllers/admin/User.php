<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

    public  function __construct()
    {
        parent::__construct();
        cek_login();
    }

    public function index()
    {
        $data['user'] = $this->db->get_where('user', ['username' => $this->session->userdata('username')])->row_array();
        $data['_view']= "admin/user/user";

        $this->load->view('template/index', $data);
    }
    public function edit()
    {
        $data['nama'] = $this->db->get_where('pegawai', ['id' => $this->session->userdata('id')])->row();
        $data['user'] = $this->db->get_where('user', ['id' => $this->session->userdata('id_user')])->row();
        $data['_view']= "admin/user/edituser";

        $this->load->view('template/index', $data);
    }
    public function edituser()
    {

        $this->form_validation->set_rules('user', 'User', 'required|trim');
        $this->form_validation->set_rules('nama', 'Nama', 'required|trim');
        $user= $this->db->get_where('user', ['id' => $this->session->userdata('id_user')])->row();
        $user1= $this->db->get_where('pegawai', ['id' => $this->session->userdata('id')])->row();
        if ($this->form_validation->run() == FALSE)
        {
            $data['nama'] = $this->db->get_where('pegawai', ['id' => $this->session->userdata('id')])->row();
            $data['user'] = $this->db->get_where('user', ['id' => $this->session->userdata('id_user')])->row();
            $data['_view']= "admin/user/edituser";

            $this->load->view('template/index', $data);
        }else{
            $data = [
                "username" => $this->input->post('user'),
            ];
            $data1 = [
                "nama" => $this->input->post('nama'),
            ];
            $this->db->where('id',$user->id);
            $this->db->update('user', $data);
            $this->db->where('id',$user1->id);
            $this->db->update('pegawai', $data1);
            $this->session->unset_userdata('nama');
            $this->session->unset_userdata('username');
            $data2 = [
                'nama' => $this->input->post('nama'),
                'username' => $this->input->post('user'),
            ];
            $this->session->set_userdata($data2);
            $this->session->set_flashdata('flash',"Username dan Nama Berhasil Diubah");
            redirect('admin/user/edit/');
        }
    }
    public function editpass()
    {
        $this->form_validation->set_rules('pass_old', 'Password', 'required|trim');
        $this->form_validation->set_rules('pass_new', 'Password', 'required|trim|min_length[4]',[
            'min_length' => 'Password harus lebih dari 4 karakter',
        ]);
        $this->form_validation->set_rules('konfir_pass', 'Password', 'matches[pass_new]',[
            'matches' => 'Password tidak sama',
        ]);
        $user= $this->db->get_where('user', ['id' => $this->session->userdata('id_user')])->row();
        $pass_old =md5($this->input->post('pass_old'));
        if ($this->form_validation->run() == FALSE)
        {
            $data['nama'] = $this->db->get_where('pegawai', ['id' => $this->session->userdata('id')])->row();
            $data['user'] = $this->db->get_where('user', ['id' => $this->session->userdata('id_user')])->row();
            $data['_view']= "admin/user/edituser";

            $this->load->view('template/index', $data);
        }
        if ($user->password == $pass_old) {
            $data = [
                "password" => md5($this->input->post('pass_new')),
            ];
            $this->db->where('id',$user->id);
            $this->db->update('user', $data);
            $this->session->set_flashdata('flash',"Password Berhasil Diubah");
            redirect('admin/user/edit/');
        }
        else{
            $this->session->set_flashdata('gagal',"Pasword Salah");
            redirect('admin/user/edit/');
        }
    }

    public function simpanfoto()
    {
        $upload_image = $_FILES['file'];
        
        $user = $this->db->get_where('user', ['id' => $this->session->userdata('id_user')])->row();

        $set_name   = $user->username;
        $path       = $_FILES['file']['name'];
        $extension  = ".".pathinfo($path, PATHINFO_EXTENSION);

        $config = array(
            'upload_path' => "./assets/img/",
            'allowed_types' => "jpg|png|jpeg|pdf",
            'overwrite' => TRUE,
            'max_size' => "2048",
            'file_name' => "$set_name".$extension,
        );

        $this->load->library('upload', $config);

        if($this->upload->do_upload('file'))
        {
            $name_file = $this->upload->data('file_name');

            $data = [
                "gambar" => $name_file,
            ];
            
            $this->db->where('id', $user->id);
            $this->db->update('user', $data);

            $this->session->set_flashdata('flash',"Gambar Berhasil perbarui");
            
            return redirect('admin/user/edit');
        }
        else
        {
            $this->session->set_flashdata('flash',$this->upload->display_errors());
            return redirect('admin/user/edit');
        }

    }
    
}