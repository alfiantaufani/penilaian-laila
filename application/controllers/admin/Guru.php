<?php

class Guru extends CI_Controller {
    public  function __construct()
    {
        parent::__construct();
        cek_login();
        $this->load->model('Guru_model');
    }

	public function index()
	{
        $data['guru'] = $this->Guru_model->getAll();
        
		$data['_view'] = "admin/guru/home";
		$this->load->view('template/index', $data);
    }

    public function detail($id)
    {
        $data['guru'] = $this->Guru_model->get($id);
        $data['_view'] = "admin/guru/detail";
		$this->load->view('template/index', $data);
    }

    public function tambah()
    {
        $this->form_validation->set_rules('nama', 'Nama', 'required|trim');
        $this->form_validation->set_rules('tmp_lahir', 'Tempat Lahir', 'required|trim');
        $this->form_validation->set_rules('tgl_lahir', 'Tanggal Lahir', 'required|trim');
        $this->form_validation->set_rules('jenis_kelamin', 'Jenis Kelamin', 'required|trim');
        $this->form_validation->set_rules('username', 'Username', 'required|trim');
        $this->form_validation->set_rules('password', 'Password', 'required|trim');

        if ($this->form_validation->run() == FALSE)
        {
            $data['_view'] = "admin/guru/tambah";
		    $this->load->view('template/index', $data);
        }
        else
        {
            $this->Guru_model->insert();
            $this->session->set_flashdata('flash',"Ditambahkan");
            redirect('admin/guru');
        }
    }

    public function edit($id)
    {
        $data['guru'] = $this->Guru_model->get($id);

        $this->form_validation->set_rules('nama', 'Nama', 'required|trim');
        $this->form_validation->set_rules('tmp_lahir', 'Tempat Lahir', 'required|trim');
        $this->form_validation->set_rules('tgl_lahir', 'Tanggal Lahir', 'required|trim');
        $this->form_validation->set_rules('jenis_kelamin', 'Jenis Kelamin', 'required|trim');

        if ($this->form_validation->run() == FALSE)
        {
            $data['_view'] = "admin/guru/edit";
		    $this->load->view('template/index', $data);
        }
        else
        {
            $this->Guru_model->update();
            $this->session->set_flashdata('flash',"Diubah");
            redirect('admin/guru');
        }
    }

    public function hapus($id)
    {
        $this->Guru_model->delete($id);
        $this->session->set_flashdata('flash',"Dihapus");
        redirect('admin/guru');
    }
}
