<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pkg extends CI_Controller {

    public  function __construct()
    {
        parent::__construct();
        cek_login();
    }

    public function index()
    {
        $queryRekapNilai = "SELECT * FROM penilaian GROUP BY id_dinilai,id_penilai,id_pegawai ORDER BY id_dinilai DESC";
        $data['rekap_nilai'] = $this->db->query($queryRekapNilai)->result();
        $data['_view']= "admin/pkg/home";

        $this->load->view('template/index', $data);
    }
    public function data_pkg()
    {
        $id =$_GET['id'];
        if ($id== "") {
            $queryRekapNilai = "SELECT * FROM penilaian GROUP BY id_dinilai,id_penilai,id_pegawai ORDER BY id_dinilai DESC";
            $hasil= $this->db->query($queryRekapNilai)->result();
        }else{
            $queryRekapNilai = "SELECT * FROM penilaian GROUP BY id_dinilai,id_penilai,id_pegawai ORDER BY id_dinilai DESC";
            $hasil = $this->db->query($queryRekapNilai)->result();
        }
        /*$data = array();
        foreach ($hasil as $field) {
            $row = array();
            $row[] = $field->id_dinilai;
            $row[] = $field->id_penilai;
            $row[] = $field->id_pegawai;
            $row[] = $field->nilai;
            $row[] = $field->id_pernyataan;
 
            $data[] = $row;
        }*/
        $output = array(
            "data" => $hasil,
        );
        echo json_encode($output);
    }

    public function detail($id,$id1)
    {
        // harus otomatis sesuai data terakhir yg belum divalidasi
        $queryKegiatan =  "SELECT * FROM komponen where type =1";
        $data['kegiatan'] = $this->db->query($queryKegiatan)->result();
        $data['id'] = $id;
        $data['id1'] = $id1;
        $data['id2'] = '';
        
        $data['_view']= "admin/pkg/detail";
        $this->load->view('template/index', $data);
    }

    public function detailkepsek($id,$id1)
    {
        // harus otomatis sesuai data terakhir yg belum divalidasi
        $queryKegiatan =  "SELECT * FROM komponen where type =2";
        $data['kegiatan'] = $this->db->query($queryKegiatan)->result();
        $data['id'] = '';
        $data['id1'] = $id1;
        $data['id2'] = $id;
        
        $data['_view']= "admin/pkg/detail";
        $this->load->view('template/index', $data);
    }

    public function cetaklaporan($penilai,$dinilai)
    {
        $data['kegiatan'] = $this->db->get_where('komponen',['type' => 1])->result();
        $role= $this->db->get_where('user',['role_id' => 3])->row();
        $data['kepsek']= $this->db->get_where('pegawai',['id' => $role->id_pegawai])->row();
        $data['dinilaian'] = $this->db->get_where('guru',['id' => $dinilai])->row();
        $data['penilai'] = $penilai;
        $data['penilai1'] = "";
        $data['dinilai'] = $dinilai;
        $guru1 =$this->db->get_where('guru',['id' => $penilai])->row();
        $guru2 =$this->db->get_where('guru',['id' => $dinilai])->row();
        $this->load->library('pdf');
        $this->pdf->setPaper('legal', 'potrait');
        $this->pdf->filename = "Penilaian".$guru1->nama."Dinilai".$guru2->nama.".pdf";
        $this->pdf->load_view('pdf/laporan_hasil_pengajuan', $data);
    }

    public function cetak_pilihan()
    {
        $dinilai=$this->input->post('penilaian');
        $role= $this->db->get_where('user',['role_id' => 3])->row();
        $data['kepsek']= $this->db->get_where('pegawai',['id' => $role->id_pegawai])->row();
        if ($dinilai == 'guru'){
            $this->db->group_by("id_dinilai");
            $this->db->where("id_pegawai",NULL);
            $this->db->order_by("id_dinilai");
            $data['penilaian'] = $this->db->get('penilaian')->result();
            $data['penilai'] = 'guru';
            $data['header'] = 'Data Penilaian Guru';
            $this->load->library('pdf');
            $this->pdf->setPaper('legal', 'potrait');
            $this->pdf->filename = "Penilaian Guru.pdf";
            $this->pdf->load_view('pdf/cetak_pilihan', $data);

        }elseif ($dinilai == 'kepsek'){
            $this->db->group_by("id_dinilai");
            $this->db->where("id_penilai",NULL);
            $this->db->order_by("id_dinilai");
            $data['penilaian'] = $this->db->get('penilaian')->result();
            $data['penilai'] = 'kepsek';
            $data['header'] = 'Data Penilaian Kepsek';
            $this->load->library('pdf');
            $this->pdf->setPaper('legal', 'potrait');
            $this->pdf->filename = "Penilaian Kepala Sekolah.pdf";
            $this->pdf->load_view('pdf/cetak_pilihan', $data);
        }
    }

    public function cetaklaporankepsek($penilai,$dinilai)
    {
        $data['kegiatan'] = $this->db->get_where('komponen',['type' => 2])->result();
        $role= $this->db->get_where('user',['role_id' => 3])->row();
        $data['kepsek']= $this->db->get_where('pegawai',['id' => $role->id_pegawai])->row();
        $data['dinilaian'] = $this->db->get_where('guru',['id' => $dinilai])->row();
        $data['penilai'] = "";
        $data['penilai1'] = $penilai;
        $data['dinilai'] = $dinilai;
        $guru1 =$this->db->get_where('guru',['id' => $dinilai])->row();
        $guru2 =$this->db->get_where('pegawai',['id' => $penilai])->row();
        $this->load->library('pdf');
        $this->pdf->setPaper('legal', 'potrait');
        $this->pdf->filename = "Penilaian ".$guru1->nama." Dinilai ".$guru2->nama.".pdf";
        $this->pdf->load_view('pdf/laporan_hasil_pengajuan', $data);
    }

    public function hapus($id){
        $this->db->delete('rekap_nilai', ['id' => $id]);
        $this->db->delete('nilai', ['rekap_nilai_id' => $id]);

        // hapus gambar belum
        $this->session->set_flashdata('flash',"Data Pengajuan berhasil dihapus");
        redirect('admin/pak');
    }
    
}