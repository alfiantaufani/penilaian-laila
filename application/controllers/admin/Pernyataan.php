<?php

class Pernyataan extends CI_Controller {
    public  function __construct()
    {
        parent::__construct();
        cek_login();
        $this->load->model('Pernyataan_model');
    }

	public function index()
	{
        $data['pernyataan'] = $this->Pernyataan_model->getAll();
        
		$data['_view'] = "admin/pernyataan/home";
		$this->load->view('template/index', $data);
    }

    public function detail($id)
    {
        $data['pernyataan'] = $this->Pernyataan_model->get($id);

        $data['_view'] = "admin/pernyataan/detail";
		$this->load->view('template/index', $data);
    }

    public function tambah()
    {
        $data['komponen'] = $this->Pernyataan_model->komponen();

        
        $this->form_validation->set_rules('komponen', 'Komponen Penilaian', 'required|trim');
        $this->form_validation->set_rules('pernyataan', 'Pernyataan Penilaian', 'required|trim');

        if ($this->form_validation->run() == FALSE)
        {
            $data['_view'] = "admin/pernyataan/tambah";
		    $this->load->view('template/index', $data);
        }
        else
        {
            $this->Pernyataan_model->insert();
            $this->session->set_flashdata('flash',"Ditambahkan");
            redirect('admin/pernyataan');
        }
    }

    public function edit($id)
    {
        $data['pernyataan'] = $this->Pernyataan_model->get($id);
        $data['komponen'] = $this->Pernyataan_model->komponen();

        
        $this->form_validation->set_rules('komponen', 'Komponen Penilaian', 'required|trim');
        $this->form_validation->set_rules('pernyataan', 'Pernyataan Penilaian', 'required|trim');

        if ($this->form_validation->run() == FALSE)
        {
            $data['_view'] = "admin/pernyataan/edit";
		    $this->load->view('template/index', $data);
        }
        else
        {
            $this->Pernyataan_model->update();
            $this->session->set_flashdata('flash',"Diubah");
            redirect('admin/pernyataan');
        }
    }

    public function hapus($id)
    {
        $this->Pernyataan_model->delete($id);
        $this->session->set_flashdata('flash',"Dihapus");
        redirect('admin/pernyataan');
    }
}
