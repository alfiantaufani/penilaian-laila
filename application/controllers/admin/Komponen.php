<?php

class Komponen extends CI_Controller {
    public  function __construct()
    {
        parent::__construct();
        cek_login();
        $this->load->model('Komponen_model');
    }

	public function index()
	{
        $data['komponen'] = $this->Komponen_model->getAll();
        
		$data['_view'] = "admin/komponen/home";
		$this->load->view('template/index', $data);
    }

    public function detail($id)
    {
        $data['komponen'] = $this->Komponen_model->get($id);

        $data['_view'] = "admin/komponen/detail";
		$this->load->view('template/index', $data);
    }

    public function tambah()
    {
        $this->form_validation->set_rules('komponen', 'Komponen Penilaian', 'required|trim');
        $this->form_validation->set_rules('type', 'Komponen Penilaian', 'required|trim');
        if ($this->form_validation->run() == FALSE)
        {
            $data['_view'] = "admin/komponen/tambah";
		    $this->load->view('template/index',$data);
        }
        else
        {
            $this->Komponen_model->insert();
            $this->session->set_flashdata('flash',"Ditambahkan");
            redirect('admin/komponen');
        }
    }

    public function edit($id)
    {
        

        
        $this->form_validation->set_rules('komponen', 'Komponen Penilaian', 'required|trim');

        if ($this->form_validation->run() == FALSE)
        {
            $data['komponen'] = $this->Komponen_model->get($id);
            $data['_view'] = "admin/komponen/edit";
		    $this->load->view('template/index', $data);
        }
        else
        {
            $this->Komponen_model->update();
            $this->session->set_flashdata('flash',"Diubah");
            redirect('admin/komponen');
        }
    }

    public function hapus($id)
    {
        $this->Komponen_model->delete($id);
        $this->session->set_flashdata('flash',"Dihapus");
        redirect('admin/komponen');
    }
}
