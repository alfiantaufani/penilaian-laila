<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pkg extends CI_Controller {

    public  function __construct()
    {
        parent::__construct();
        cek_login();
    }

    public function index()
    {
        $id = $this->session->userdata('id');
        $queryRekapNilai = "SELECT * FROM penilaian JOIN guru ON penilaian.id_dinilai = guru.id where penilaian.id_pegawai = $id GROUP BY penilaian.id_dinilai ORDER BY penilaian.id DESC";
        $data['rekap_nilai'] = $this->db->query($queryRekapNilai)->result_array();

        $data['_view']= "kepsek/pkg/home";

        $this->load->view('template/index', $data);
    }

    public function penilaian()
    {
        $queryKegiatan =  "SELECT * FROM komponen where type =2";
        $data['kegiatan'] = $this->db->query($queryKegiatan)->result();
        $data['_view']= "kepsek/pkg/penilaian";
        $this->load->view('template/index', $data);
    }
    public function cetaklaporan($id)
    {
        // echo $_SERVER["DOCUMENT_ROOT"].'/pkg/assets/img/logodinas.png'; die;
        $queryKegiatan = "SELECT `kegiatan`.`id` as kegiatan_id, `nilai`.`id` as nilai_id, `nilai`.`status`,
        `nilai`.`alasan`, `nilai`.`saran`, `nilai`.`judul`, `kegiatan`.`unsur_id`, `kegiatan`.`kode`, 
        `kegiatan`.`kegiatan`, `kegiatan`.`satuan`, 
        `kegiatan`.`angka_kredit`, `kegiatan`.`pelaksana`,
        `unsur`.`unsur`, `unsur`.`sub_unsur`, 
        `nilai`.`file`, `nilai`.`rekap_nilai_id` as `rekap_nilai_id`
        FROM `kegiatan` 
        JOIN `unsur`
        ON `kegiatan`.`unsur_id` = `unsur`.`id`
        JOIN `nilai`
        ON `kegiatan`.`id` = `nilai`.`kegiatan_id`
        WHERE `nilai`.`rekap_nilai_id` = $id
        ORDER BY `kegiatan`.`id` 
        ";

        $data['kegiatan'] = $this->db->query($queryKegiatan)->result();

        $data['pendaftar'] = $this->db->get_where('pendaftar',['id' => $this->session->userdata('id')])->row();

        $this->load->library('pdf');
        $this->pdf->setPaper('legal', 'potrait');
        $this->pdf->filename = "laporan hasil pengajuan.pdf";
        $this->pdf->load_view('pdf/laporan_hasil_pengajuan', $data);
    }
    public function simpan()
    {
        $pernyataan = $this->db->get('pernyataan')->result_array();
        $penilai = $this->session->userdata('id');
        $dinilai =$this->input->post('id');
        $this->db->select('*');
        $this->db->where('id_pegawai',$penilai);
        $this->db->where('id_dinilai',$dinilai);
        $cek = $this->db->get('penilaian')->row();
        if ($cek > 0) {

        }else{
            foreach ($pernyataan as $key => $value) {
                $nilai = $this->input->post($value['id']);
                if ($nilai == "") {
                    
                }else{
                    $data = [
                        "id_pegawai" => $penilai,
                        "id_dinilai" => $dinilai,
                        "id_pernyataan" => $value['id'],
                        "nilai" => $nilai,
                        "tgl" => time()
                    ];
                    $this->db->insert('penilaian', $data);
                }
            }
        }
        redirect('kepsek/pkg');

    }
    public function detail($id)
    {
        $data['kegiatan']= $this->db->get_where('komponen',['type' => 2])->result();
        $data['id']= $id;
        $data['_view']= "kepsek/pkg/detail";
        $this->load->view('template/index', $data);
    }

}