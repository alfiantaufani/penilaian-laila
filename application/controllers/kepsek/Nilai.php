<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Nilai extends CI_Controller {

    public  function __construct()
    {
        parent::__construct();
        cek_login();
    }

    public function index()
    {
        $queryRekapNilai = "SELECT * FROM penilaian GROUP BY id_dinilai,id_penilai,id_pegawai ORDER BY id_dinilai DESC";
        $data['rekap_nilai'] = $this->db->query($queryRekapNilai)->result();
        $data['_view']= "kepsek/nilai/home";

        $this->load->view('template/index', $data);
    }
    public function cetaklaporan($id)
    {
        // echo $_SERVER["DOCUMENT_ROOT"].'/pak/assets/img/logodinas.png'; die;
        $queryKegiatan = "SELECT `kegiatan`.`id` as kegiatan_id, `nilai`.`id` as nilai_id, `nilai`.`status`,
        `nilai`.`alasan`, `nilai`.`saran`, `nilai`.`judul`, `kegiatan`.`unsur_id`, `kegiatan`.`kode`, 
        `kegiatan`.`kegiatan`, `kegiatan`.`satuan`, 
        `kegiatan`.`angka_kredit`, `kegiatan`.`pelaksana`,
        `unsur`.`unsur`, `unsur`.`sub_unsur`, 
        `nilai`.`file`, `nilai`.`rekap_nilai_id` as `rekap_nilai_id`
        FROM `kegiatan` 
        JOIN `unsur`
        ON `kegiatan`.`unsur_id` = `unsur`.`id`
        JOIN `nilai`
        ON `kegiatan`.`id` = `nilai`.`kegiatan_id`
        WHERE `nilai`.`rekap_nilai_id` = $id
        ORDER BY `kegiatan`.`id` 
        ";

        $data['kegiatan'] = $this->db->query($queryKegiatan)->result();

        $data['pendaftar'] = $this->db->get_where('pendaftar',['id' => $this->session->userdata('id')])->row();

        $this->load->library('pdf');
        $this->pdf->setPaper('legal', 'potrait');
        $this->pdf->filename = "laporan hasil pengajuan.pdf";
        $this->pdf->load_view('pdf/laporan_hasil_pengajuan', $data);
    }
    public function detail($id,$id1)
    {
        // harus otomatis sesuai data terakhir yg belum divalidasi
        $queryKegiatan =  "SELECT * FROM komponen where type =1";
        $data['kegiatan'] = $this->db->query($queryKegiatan)->result();
        $data['id'] = $id;
        $data['id1'] = $id1;
        $data['id2'] = '';
        
        $data['_view']= "admin/pkg/detail";
        $this->load->view('template/index', $data);
    }

    public function detailkepsek($id,$id1)
    {
        // harus otomatis sesuai data terakhir yg belum divalidasi
        $queryKegiatan =  "SELECT * FROM komponen where type =2";
        $data['kegiatan'] = $this->db->query($queryKegiatan)->result();
        $data['id'] = '';
        $data['id1'] = $id1;
        $data['id2'] = $id;
        
        $data['_view']= "admin/pkg/detail";
        $this->load->view('template/index', $data);
    }

}