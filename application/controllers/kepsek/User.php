<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

    public  function __construct()
    {
        parent::__construct();
        $this->load->model('Guru_model');
        cek_login();
    }

    public function index()
    {
        $data['user'] = $this->db->get_where('user', ['username' => $this->session->userdata('username')])->row_array();
        $data['_view']= "kepsek/user/home";

        $this->load->view('template/index', $data);
    }

    public function edit($id)
    {

        $this->form_validation->set_rules('nip', 'NIP', 'required|trim');
        $this->form_validation->set_rules('nama', 'Nama', 'required|trim');
        $this->form_validation->set_rules('tmp_lahir', 'Tempat Lahir', 'required|trim');
        $this->form_validation->set_rules('tgl_lahir', 'Tanggal Lahir', 'required|trim');
        $this->form_validation->set_rules('jenis_kelamin', 'Jenis Kelamin', 'required|trim');

        $data['guru'] = $this->db->get_where('pegawai', ['id' => $id])->row();

        if ($this->form_validation->run() == FALSE)
        {
            $data['_view']= "kepsek/user/edit";
            $this->load->view('template/index', $data);
        }
        else
        {
            $data = [
                "nip"           => $this->input->post('nip', true),
                "nama"          => $this->input->post('nama', true),
                "no_karpeg"     => $this->input->post('no_karpeg'),
                "jenis_kelamin" => $this->input->post('jenis_kelamin', true),
                "tmp_lahir"     => $this->input->post('tmp_lahir', true),
                "tgl_lahir"     => $this->input->post('tgl_lahir', true),
                "pendidikan_terakhir" => $this->input->post('pendidikan_terakhir'),
                "pangkat"       => $this->input->post('pangkat'),
                "nrg"           => $this->input->post('nrg'),
                "nuptk"         => $this->input->post('nuptk'),
                "mapel"         => $this->input->post('mapel'),
                "jam_mengajar"  => $this->input->post('jam_mengajar'),
            ];
            $this->db->where('id', $id);
            $this->db->update('pegawai', $data);
            $this->session->set_flashdata('flash',"Diubah");
            redirect('kepsek/user/edit/'.$id);
            }
        }

        public function simpanfoto()
        {
            $upload_image = $_FILES['file'];

            $user = $this->db->get_where('user', ['id' => $this->session->userdata('id_user')])->row();


            $set_name   = $user->username;
            $path       = $_FILES['file']['name'];
            $extension  = ".".pathinfo($path, PATHINFO_EXTENSION);

            $config = array(
                'upload_path' => "./assets/img/",
                'allowed_types' => "jpg|png|jpeg|pdf",
                'overwrite' => TRUE,
                'max_size' => "2048",
                'file_name' => "$set_name".$extension,
            );

            $this->load->library('upload', $config);

            if($this->upload->do_upload('file'))
            {
                $name_file = $this->upload->data('file_name');

                $data = [
                    "gambar" => $name_file,
                ];

                $this->db->where('id', $user->id);
                $this->db->update('user', $data);

                $this->session->set_flashdata('flash',"Gambar Berhasil perbarui");

                return redirect('kepsek/user/edit/'.$this->session->userdata('id'));
            }
            else
            {
                $this->session->set_flashdata('flash',$this->upload->display_errors());
                return redirect('kepsek/user/edit/'.$this->session->userdata('id'));
            }

        }

    }