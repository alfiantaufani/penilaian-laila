<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

    public  function __construct()
    {
        parent::__construct();
        $this->load->model('Guru_model');
        cek_login();
    }

    public function index()
    {
        $data['user'] = $this->db->get_where('user', ['username' => $this->session->userdata('username')])->row_array();
        $data['_view']= "guru/user/home";

        $this->load->view('template/index', $data);
    }

    public function edit($id)
    {

        $this->form_validation->set_rules('nip', 'NIP', 'required|trim');
        $this->form_validation->set_rules('nama', 'Nama', 'required|trim');
        $this->form_validation->set_rules('tmp_lahir', 'Tempat Lahir', 'required|trim');
        $this->form_validation->set_rules('tgl_lahir', 'Tanggal Lahir', 'required|trim');
        $this->form_validation->set_rules('jenis_kelamin', 'Jenis Kelamin', 'required|trim');

        $data['guru'] = $this->db->get_where('guru', ['id' => $id])->row();

        if ($this->form_validation->run() == FALSE)
        {
            $data['_view']= "guru/user/edit";
            $this->load->view('template/index', $data);
        }
        else
        {
            $this->Guru_model->update($id);
            $this->session->set_flashdata('flash',"Diubah");
            redirect('guru/user/edit/'.$id);
        }
    }

    public function simpanfoto()
    {
        $upload_image = $_FILES['file'];
        
        $user = $this->db->get_where('user', ['id' => $this->session->userdata('id_user')])->row();


        $set_name   = $user->username;
        $path       = $_FILES['file']['name'];
        $extension  = ".".pathinfo($path, PATHINFO_EXTENSION);

        $config = array(
            'upload_path' => "./assets/img/",
            'allowed_types' => "jpg|png|jpeg|pdf",
            'overwrite' => TRUE,
            'max_size' => "2048",
            'file_name' => "$set_name".$extension,
        );

        $this->load->library('upload', $config);

        if($this->upload->do_upload('file'))
        {
            $name_file = $this->upload->data('file_name');

            $data = [
                "gambar" => $name_file,
            ];
            
            $this->db->where('id', $user->id);
            $this->db->update('user', $data);

            $this->session->set_flashdata('flash',"Gambar Berhasil perbarui");
            
            return redirect('guru/user/edit/'.$this->session->userdata('id'));
        }
        else
        {
            $this->session->set_flashdata('flash',$this->upload->display_errors());
            return redirect('guru/user/edit/'.$this->session->userdata('id'));
        }

    }

}