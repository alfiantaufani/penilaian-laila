<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pkg extends CI_Controller {

    public  function __construct()
    {
        parent::__construct();
        cek_login();
    }

    public function index()
    {
        $id = $this->session->userdata('id');
        $queryRekapNilai = "SELECT * FROM penilaian JOIN guru ON penilaian.id_dinilai = guru.id where penilaian.id_penilai = $id GROUP BY penilaian.id_dinilai ORDER BY penilaian.id DESC";
        $data['rekap_nilai'] = $this->db->query($queryRekapNilai)->result_array();

        $data['_view']= "guru/pkg/home";

        $this->load->view('template/index', $data);
    }

    public function penilaian()
    {
        $queryKegiatan =  "SELECT * FROM komponen WHERE type = 1 ";
        $data['kegiatan'] = $this->db->query($queryKegiatan)->result();
        $data['_view']= "guru/pkg/penilaian";
        $this->load->view('template/index', $data);
    }
    public function cetaklaporan($id)
    {
        $data['kegiatan'] = $this->db->get_where('komponen',['type' => 2])->result();

        $data['pendaftar'] = $this->db->get_where('guru',['id' => $this->session->userdata('id')])->row();

        $this->load->library('pdf');
        $this->pdf->setPaper('legal', 'potrait');
        $this->pdf->filename = "laporan hasil pengajuan.pdf";
        $this->pdf->load_view('pdf/laporan_hasil_pengajuan', $data);
    }
    public function simpan()
    {
        $pernyataan = $this->db->get('pernyataan')->result_array();
        $penilai = $this->session->userdata('id');
        $dinilai =$this->input->post('id');
        $this->db->select('*');
        $this->db->where('id_penilai',$penilai);
        $this->db->where('id_dinilai',$dinilai);
        $cek = $this->db->get('penilaian')->row();
        if ($cek > 0) {

        }else{
            foreach ($pernyataan as $key => $value) {
                $nilai = $this->input->post($value['id']);
                if ($nilai == "") {
                    
                }else{
                    $data = [
                        "id_penilai" => $penilai,
                        "id_dinilai" => $dinilai,
                        "id_pernyataan" => $value['id'],
                        "nilai" => $nilai,
                        "tgl" => time()
                    ];
                    $this->db->insert('penilaian', $data);
                }
            }
        }
        redirect('guru/pkg');

    }
    public function detail($id)
    {
        $data['kegiatan']= $this->db->get_where('komponen',['type' => 1])->result();
        $data['id']= $id;
        $data['_view']= "guru/pkg/detail";
        $this->load->view('template/index', $data);
    }

}