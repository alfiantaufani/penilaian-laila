<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Nilai extends CI_Controller {

    public  function __construct()
    {
        parent::__construct();
        cek_login();
    }

    public function index()
    {
        $id = $this->session->userdata('id');
        $queryRekapNilai = "SELECT * FROM penilaian LEFT JOIN guru ON penilaian.id_penilai = guru.id where penilaian.id_dinilai = $id GROUP BY penilaian.id_penilai,penilaian.id_pegawai ORDER BY penilaian.id DESC";
        $data['rekap_nilai'] = $this->db->query($queryRekapNilai)->result();
        $data['_view']= "guru/nilai/home";

        $this->load->view('template/index', $data);
    }
    public function cetaklaporan($id)
    {
        // echo $_SERVER["DOCUMENT_ROOT"].'/pak/assets/img/logodinas.png'; die;
        $queryKegiatan = "SELECT `kegiatan`.`id` as kegiatan_id, `nilai`.`id` as nilai_id, `nilai`.`status`,
        `nilai`.`alasan`, `nilai`.`saran`, `nilai`.`judul`, `kegiatan`.`unsur_id`, `kegiatan`.`kode`, 
        `kegiatan`.`kegiatan`, `kegiatan`.`satuan`, 
        `kegiatan`.`angka_kredit`, `kegiatan`.`pelaksana`,
        `unsur`.`unsur`, `unsur`.`sub_unsur`, 
        `nilai`.`file`, `nilai`.`rekap_nilai_id` as `rekap_nilai_id`
        FROM `kegiatan` 
        JOIN `unsur`
        ON `kegiatan`.`unsur_id` = `unsur`.`id`
        JOIN `nilai`
        ON `kegiatan`.`id` = `nilai`.`kegiatan_id`
        WHERE `nilai`.`rekap_nilai_id` = $id
        ORDER BY `kegiatan`.`id` 
        ";

        $data['kegiatan'] = $this->db->query($queryKegiatan)->result();

        $data['pendaftar'] = $this->db->get_where('pendaftar',['id' => $this->session->userdata('id')])->row();

        $this->load->library('pdf');
        $this->pdf->setPaper('legal', 'potrait');
        $this->pdf->filename = "laporan hasil pengajuan.pdf";
        $this->pdf->load_view('pdf/laporan_hasil_pengajuan', $data);
    }
    public function detail($id)
    {
        $data['kegiatan']= $this->db->get_where('komponen',['type' => 1])->result();
        $data['id']= $id;
        $data['id_kepsek']= '';
        $data['_view']= "guru/nilai/detail";
        $this->load->view('template/index', $data);
    }
    public function detailkepsek($id)
    {
        $data['kegiatan']= $this->db->get_where('komponen',['type' => 2])->result();
        $data['id_kepsek']= $id;
        $data['id']= '';
        $data['_view']= "guru/nilai/detail";
        $this->load->view('template/index', $data);
    }

}