<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {

    public function index()
    {
        if($this->session->userdata('role_id') == 1){
            redirect('admin/home');
        } else if($this->session->userdata('role_id') == 2){
            redirect('guru/home');
        } else if($this->session->userdata('role_id') == 3){
            redirect('kepsek/home');
        }

        $this->form_validation->set_rules('username', 'Username', 'required|trim');
        $this->form_validation->set_rules('password', 'Password', 'required|trim');

        if ($this->form_validation->run() == FALSE) {
            $this->load->view('auth/login');
        } else {
            // menuju fungsi Login
            $this->_login();
        }
    }

    private function _login()
    {
        $username = $this->input->post('username', true);
        $password = $this->input->post('password');

        $user = $this->db->get_where('user',['username' => $username])->row();
        
        // jika username ada
        if($user) {
            if($user->id_pegawai !== null){
                $dataUser = $this->db->get_where('pegawai',['id' => $user->id_pegawai])->row();
            }else{
                $dataUser = $this->db->get_where('guru',['id' => $user->id_guru])->row();
            }
            // cek password
            if(md5($password, $user->password)) {

                $data = [
                    'id' => $dataUser->id,
                    'nama' => $dataUser->nama,
                    'username' => $user->username,
                    'gambar' => $user->gambar,
                    'role_id' => $user->role_id,
                    'id_user' => $user->id,
                ];
                $this->session->set_userdata($data);

                if($user->role_id == 1){
                    redirect('admin/home');
                } else if($user->role_id == 2){
                    redirect('guru/home');
                } else if($user->role_id == 3){
                    redirect('kepsek/home');
                }

            } else {
                // pesan
                $this->session->set_flashdata('pesan','<div class="alert alert-danger" role="alert">Password Salah!</div>');
                redirect('auth');
            }
        } else {
            // pesan 
            $this->session->set_flashdata('pesan','<div class="alert alert-danger" role="alert">Username tidak terdaftar!</div>');
            redirect('auth');
        }
    }

    public function registrasi()
    {
        $this->form_validation->set_rules('nama', 'Nama', 'required|trim');
        $this->form_validation->set_rules('tmp_lahir', 'Tempat Lahir', 'required|trim');
        $this->form_validation->set_rules('tgl_lahir', 'Tanggal Lahir', 'required|trim');
        $this->form_validation->set_rules('username', 'Tanggal Lahir', 'required|trim');
        $this->form_validation->set_rules('jenis_kelamin', 'Jenis Kelamin', 'required|trim');
        $this->form_validation->set_rules('password1', 'Password', 'required|trim|min_length[4]|matches[password2]',[
            'matches' => 'Password tidak sama',
            'min_length' => 'Password harus lebih dari 4 karakter',
        ]);
        $this->form_validation->set_rules('password2', 'Password', 'matches[password1]');

        if ($this->form_validation->run() == FALSE)
        {
            $this->load->view('auth/registrasi');
        }
        else
        {
            $data1 = [
                "nama"          => $this->input->post('nama', true),
                "no_karpeg"     => $this->input->post('no_karpeg'),
                "jenis_kelamin" => $this->input->post('jenis_kelamin', true),
                "tmp_lahir"     => $this->input->post('tmp_lahir', true),
                "tgl_lahir"     => $this->input->post('tgl_lahir', true),
                "pendidikan_terakhir" => $this->input->post('pendidikan_terakhir'),
                "pangkat"       => $this->input->post('pangkat'),
                "nrg"           => $this->input->post('nrg'),
                "nuptk"         => $this->input->post('nuptk'),
            ];
            $this->db->insert('guru', $data1);

            $data2 = [
                "id_guru" => $this->db->insert_id(),
                "username" => $this->input->post('username', true),
                "password" => md5($this->input->post('password1')),
                "gambar" => "avatar.jpg",
                "role_id" => 2,
                "is_active" => 1,
                "date_created" => time(),
            ];
            $this->db->insert('user', $data2);
            $this->session->set_flashdata('pesan','<div class="alert alert-success" role="alert">Registrasi Berhasil</div>');
            redirect('/');
        }
        
    }

    public function forbidden()
    {
        $this->load->view('auth/403');
    }

    public function logout()
    {
        $this->session->unset_userdata('nama');
        $this->session->unset_userdata('username');
        $this->session->unset_userdata('gambar');
        $this->session->unset_userdata('role_id');
        $this->session->unset_userdata('id_user');

        $this->session->set_flashdata('pesan','<div class="alert alert-success" role="alert">Logout Berhasil</div>');
        redirect('/');
    }
    
}