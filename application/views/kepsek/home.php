<div class="container-fluid">
    <h1 class="h3 mb-4 text-gray-800">Selamat Datang Di Aplikasi PENILAIAN KINERJA GURU </h1>

    <div class="row">
        <div class="col-md-12">
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                  <h6 class="m-0 font-weight-bold text-primary">Data Guru</h6>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-8">
                            <table style="font-size:18px">
                                <tr>
                                    <td>NIP</td>
                                    <td>: <?= $guru->nip ?></td>
                                </tr>
                                <tr>
                                    <td>NUPTK</td>
                                    <td>: <?= $guru->nuptk ?></td>
                                </tr>
                                <tr>
                                    <td>KARPEG</td>
                                    <td>: <?= $guru->no_karpeg ?></td>
                                </tr>
                                <tr>
                                    <td>Nama</td>
                                    <td>: <?= $guru->nama ?></td>
                                </tr>
                                <tr>
                                    <td>TTL</td>
                                    <td>: <?= $guru->tmp_lahir.', '.$guru->tgl_lahir ?></td>
                                </tr>
                                <tr>
                                    <td>Jenis Kelamin</td>
                                    <td>: <?= $guru->jenis_kelamin ?></td>
                                </tr>
                                <tr>
                                    <td>Pendidikan Terakhir</td>
                                    <td>: <?= $guru->pendidikan_terakhir ?></td>
                                </tr>
                                <tr>
                                    <td>Pangkat/Golongan</td>
                                    <td>: <?= $guru->pangkat ?></td>
                                </tr>
                                <tr>
                                    <td>NRG</td>
                                    <td>: <?= $guru->nrg ?></td>
                                </tr>
                            </table>
                        </div>
                        <div class="col-md-3">
                            <?php $datagambar = $this->db->get_where('user', ['username' => $this->session->userdata('username')])->row() ?>
                            
                            <img src="<?= base_url('assets/img/'.$datagambar->gambar); ?>" class="card-img" alt="<?= $this->session->userdata('nama'); ?>">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>