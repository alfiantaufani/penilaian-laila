<div class="container-fluid">

    <h1 class="h3 mb-2 text-gray-800">Data Nilai</h1>

    <div class="card shadow mb-4">
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama Penilai</th>
                            <th>Tanggal Nilai</th>
                            <th>Jumlah Nilai</th>
                            <th>Rata Rata</th>
                            <th>Option</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $i=1; foreach($rekap_nilai as  $r) :  ?>
                        <tr>
                            <td><?= $i++; ?></td>
                            <td><?php echo $this->db->get_where('guru',['id' => $r->id_dinilai])->row()->nama; ?></td>
                            <td><?= date('d F Y',$r->tgl); ?></td>
                            <?php
                            $this->db->select('sum(nilai) as ttl');
                            $this->db->where('id_penilai',$r->id_penilai);
                            $this->db->where('id_dinilai',$r->id_dinilai);
                            $ttl_nilai = $this->db->get('penilaian')->row(); ?>
                            <td><?= $ttl_nilai->ttl; ?></td>
                            <?php
                            $this->db->select('count(nilai) as ttl');
                            $this->db->where('id_penilai',$r->id_penilai);
                            $this->db->where('id_dinilai',$r->id_dinilai);
                            $jml_nilai = $this->db->get('penilaian')->row(); 
                            $total =$ttl_nilai->ttl / $jml_nilai->ttl;?>
                            <td><?= $total; ?></td>
                            <?php if (empty($r->id_penilai)): ?>
                                <td>
                                    <a href="<?= base_url('kepsek/nilai/detailkepsek/'.$r->id_pegawai.'/'.$r->id_dinilai) ?>" class="badge badge-info float-right mr-1 mb-1">Detail</a>
                                    <a href="<?= base_url('kepsek/nilai/cetaklaporankepsek/'.$r->id_pegawai.'/'.$r->id_dinilai) ?>" class="badge badge-warning float-right mr-1" target="_blank">Cetak</a>
                                </td>
                                <?php else :?>
                                    <td>
                                        <a href="<?= base_url('kepsek/nilai/detail/'.$r->id_penilai.'/'.$r->id_dinilai) ?>" class="badge badge-info float-right mr-1 mb-1">Detail</a>
                                        <!-- <a href="<?= base_url('kepsek/nilai/cetaklaporan/'.$r->id_penilai.'/'.$r->id_dinilai) ?>" class="badge badge-warning float-right mr-1" target="_blank">Cetak</a> -->
                                    </td>
                                <?php endif; ?>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

</div>