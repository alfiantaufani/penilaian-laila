<!-- Begin Page Content -->
<div class="container-fluid">

    <div class="row">
        <div class="col-md-4">
            <h1 class="h3 mb-4 text-gray-800">Form Pengajuan</h1>
        </div>
        <div class="col-md-8">
            <div class="card">
                <center>
                    <span>Petunjuk Penilaian</span><br>
                    <span class="row">
                        <span class="col-md-4">0 = Tidak Terpenuhi <br>(1-60)</span>
                        <span class="col-md-4">1 = Hampir Terpenuhi<br>(61-74)</span>
                        <span class="col-md-4">2 = Terpenuhi <br>(75-100)</span>
                    </span>
                </center>  
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 form-group">
            <select  data-placeholder="Masukkan Nama Lengkap" class="form-control" id="itemName" style="width: 100%;" name="nama">
                <option value=""></option>
                <?php
                $hh =$this->session->userdata('id');
                $query2 = "SELECT * FROM guru order by nama";
                $hasil2 = $this->db->query($query2)->result_array();
                foreach ($hasil2 as $key => $value) {
                    ?>
                    <option value="<?php echo $value['id'] ?>,<?php echo $value['nama'] ?>"><?php echo $value['nama'] ?></option>
                    <?php
                }
                ?>
            </select>
        </div>
    </div>

    <?php echo form_open_multipart('kepsek/pkg/simpan');?>
    <div class="card shadow mb-4 abcdefg"  style="display: none;">
        <div class="card-header py-3" id="hidden"></div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-hover table-bordered" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <td width="10px">No</td>
                            <td colspan="2">Indikator</td>
                            <td width="170px">Nilai</td>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $i=1; foreach($kegiatan as $k): ?>
                        <tr>
                            <td><?= $i++; ?></td>
                            <td colspan="3"><?= $k->nama; ?></td>
                        </tr>
                        <?php $datapernyataan = $this->db->get_where('pernyataan', ['id_komponen' => $k->id])->result();
                        foreach($datapernyataan as $d): ?>
                            <tr>
                                <td></td>
                                <td width="10px">A</td>
                                <td>
                                    <div>
                                        <label class="form-check-label" for="<?= $d->pernyataan ?>">
                                            <?= $d->pernyataan ?>
                                        </label>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-check" style="width: 120px;">
                                        <input style="" class="form-check-input" type="radio" name="<?= $d->id?>" value="40" <?php if(set_value($d->id) == '40') echo "checked";  ?>>
                                        <label class="form-check-label" for="C">
                                            0
                                        </label>
                                        <input style="margin-left: 15px" class="form-check-input" type="radio" name="<?= $d->id?>" value="70" <?php if(set_value($d->id) == '70') echo "checked";  ?>>
                                        <label  style="margin-left: 30px" class="form-check-label" for="B">
                                            1
                                        </label>
                                        <input style="margin-left: 15px" class="form-check-input" type="radio" name="<?= $d->id?>" value="100" <?php if(set_value($d->id) == '100') echo "checked";  ?>>
                                        <label style="margin-left: 30px" class="form-check-label" for="A">
                                            2
                                        </label>
                                    </div>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
    <div class="col-md-3" style="margin-left: 10px;">
        <div class="row">
            <div class="col-xs-6 mb-3">
                <button type="submit" name="simpan" class="btn btn-primary">Simpan</button>
            </div>
            <div class="col-xs-6 mb-3" style="margin-left: 10px;">
                <a href="<?= base_url('kepsek/pkg') ?>" class="btn btn-secondary">Kembali</a>
            </div>
        </div>
    </div>
</div>

<?php echo form_close();?>

</div>
    <!-- /.container-fluid -->