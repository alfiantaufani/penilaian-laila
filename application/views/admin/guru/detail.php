<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-2 text-gray-800">Detail Data Guru</h1>
    <div class="row">
        <div class="col-md-7 mt-3">
            <div class="card" style="width: 30rem;">
                <div class="card-body">
                    <h5 class="card-title"><?= $guru->nama ?></h5>
                    <p class="card-text">
                        NIP : <?= $guru->nip ?> <br>
                        Nama : <?= $guru->nama ?> <br>
                        TTL : <?= $guru->tmp_lahir.', '.$guru->tgl_lahir ?> <br>
                        Jenis Kelamin : <?= $guru->jenis_kelamin ?> <br>
                        Status : <?= $guru->status ?> <br>
                        Agama : <?= $guru->agama ?> <br>
                        Jabatan : <?= $guru->jabatan ?> <br>
                        Telepon : <?= $guru->telepon ?> <br>
                        Alamat : <?= $guru->alamat ?> <br>
                    </p>
                    <a href="<?= base_url('admin/guru') ?>" class="card-link">Kembali</a>
                </div>
            </div>
        </div>
    </div>
</div>    