<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-2 text-gray-800">Tambah Data Guru</h1>
    <div class="row mb-5">
        <div class="col-md-12 mt-3">
            <div class="card">
                <div class="card-header">
                    Form Guru
                </div>
                <div class="card-body">
                    <form method="post" action="">
                        <div class="row"><!-- 
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="nip">NIP</label>
                                    <input type="text" class="form-control" id="nip" placeholder="NIP" name="nip" autocomplete="off" value="<?= set_value('nip'); ?>">
                                    <?= form_error('nip','<small class="text-danger">','</small>'); ?>
                                </div>
                            </div> -->
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="nama">Nama</label>
                                    <input type="text" class="form-control" id="nama" placeholder="Nama" name="nama" autocomplete="off" value="<?= set_value('nama'); ?>">
                                    <?= form_error('nama','<small class="text-danger">','</small>'); ?>
                                </div>
                            </div> 
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="nama">Nomor Seri Karpeg</label>
                                    <input type="text" class="form-control" id="no_karpeg" placeholder="Nama" name="no_karpeg" autocomplete="off" value="<?= set_value('no_karpeg'); ?>">
                                    <?= form_error('no_karpeg','<small class="text-danger">','</small>'); ?>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="jenis_kelamin">Jenis Kelamin</label>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="jenis_kelamin" id="L" value="L" <?php if(set_value('jenis_kelamin') == 'L') echo "checked";  ?>>
                                        <label class="form-check-label" for="L">
                                            Laki-Laki
                                        </label>
                                        <input style="margin-left: 15px" class="form-check-input" type="radio" name="jenis_kelamin" id="P" value="P" <?php if(set_value('jenis_kelamin') == 'P') echo "checked";  ?>>
                                        <label  style="margin-left: 30px" class="form-check-label" for="P">
                                            Perempuan
                                        </label>
                                    </div>
                                </div>
                                <?= form_error('jenis_kelamin','<small class="text-danger">','</small>'); ?>
                            </div>                           
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="tmp_lahir">Tempat Lahir</label>
                                    <input type="text" class="form-control" id="tmp_lahir" placeholder="Tempat Lahir" name="tmp_lahir" autocomplete="off" value="<?= set_value('tmp_lahir'); ?>">
                                    <?= form_error('tmp_lahir','<small class="text-danger">','</small>'); ?>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="tgl_lahir">Tanggal Lahir</label>
                                    <input type="date" class="form-control" id="tgl_lahir" placeholder="Tempat Lahir" name="tgl_lahir" autocomplete="off" value="<?= set_value('tgl_lahir'); ?>">
                                    <?= form_error('tgl_lahir','<small class="text-danger">','</small>'); ?>
                                </div>
                            </div> 
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="nama">Pendidikan Terakhir</label>
                                    <input type="text" class="form-control" id="pendidikan_terakhir" placeholder="Nama" name="pendidikan_terakhir" autocomplete="off" value="<?= set_value('pendidikan_terakhir'); ?>">
                                    <?= form_error('pendidikan_terakhir','<small class="text-danger">','</small>'); ?>
                                </div>
                            </div> 
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="nama">Pangkat / Golongan</label>
                                    <input type="text" class="form-control" id="pangkat" placeholder="Nama" name="pangkat" autocomplete="off" value="<?= set_value('pangkat'); ?>">
                                    <?= form_error('pangkat','<small class="text-danger">','</small>'); ?>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="nama">NRG</label>
                                    <input type="text" class="form-control" id="nrg" placeholder="Nama" name="nrg" autocomplete="off" value="<?= set_value('nrg'); ?>">
                                    <?= form_error('nrg','<small class="text-danger">','</small>'); ?>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="nama">NUPTK</label>
                                    <input type="text" class="form-control" id="nuptk" placeholder="Nama" name="nuptk" autocomplete="off" value="<?= set_value('nuptk'); ?>">
                                    <?= form_error('nuptk','<small class="text-danger">','</small>'); ?>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="nama">Mata Pelajaran / Kelas</label>
                                    <input type="text" class="form-control" id="mapel" placeholder="Nama" name="mapel" autocomplete="off" value="<?= set_value('mapel'); ?>">
                                    <?= form_error('mapel','<small class="text-danger">','</small>'); ?>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="nama">Jam Mengajar</label>
                                    <input type="text" class="form-control" id="jam_mengajar" placeholder="Nama" name="jam_mengajar" autocomplete="off" value="<?= set_value('jam_mengajar'); ?>">
                                    <?= form_error('jam_mengajar','<small class="text-danger">','</small>'); ?>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="username">Username</label>
                                    <input type="text" class="form-control" id="username" placeholder="Username" name="username" autocomplete="off" value="<?= set_value('username'); ?>">
                                    <?= form_error('username','<small class="text-danger">','</small>'); ?>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="password">Password</label>
                                    <input type="password" class="form-control" placeholder="Password" name="password" autocomplete="off">
                                    <?= form_error('password','<small class="text-danger">','</small>'); ?>
                                </div>
                            </div>
                        </div>
                        <button type="submit" name="simpan" class="btn btn-primary">Save</button>
                        <a href="<?= base_url('admin/guru') ?>" class="btn btn-secondary">Kembali</a>
                    </form>
                </div>
            </div>
        </div>
    </div>

</div>