<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-2 text-gray-800">Edit Data Komponen Penilaian</h1>
    <div class="row mb-5">
        <div class="col-md-12 mt-3">
            <div class="card">
                <div class="card-header">
                    Form Komponen Penilaian
                </div>
                <div class="card-body">
                    <form method="post" action="">
                        <input type="hidden" name="id" value="<?= $komponen->type; ?>" >
                        <div class="form-group">
                            <label for="komponen">Komponen</label>
                            <textarea name="komponen" id="komponen" class="form-control" autocomplete="off"><?= set_value('komponen') ? set_value('komponen') : $komponen->nama; ?></textarea>
                            <?= form_error('komponen','<small class="text-danger">','</small>'); ?>
                        </div>
                        <div class="form-group">
                            <label for="type">Type</label>
                            <select name="type" class="form-control">
                                <?php if ($komponen->type === 1) {
                                   echo '<option value="1" selected>Guru</option>
                                        ';
                                }elseif ($komponen->type === 2) {
                                    echo '
                                <option value="2"selected>Kepala Sekolah</option>';
                                }else{
                                    echo '<option value="1">Guru</option>
                                <option value="2">Kepala Sekolah</option>';
                                } ?>
                            </select>
                        </div>
                        <button type="submit" name="simpan" class="btn btn-primary">Save</button>
                        <a href="<?= base_url('admin/komponen') ?>" class="btn btn-secondary">Kembali</a>
                    </form>
                </div>
            </div>
        </div>
    </div>

</div>