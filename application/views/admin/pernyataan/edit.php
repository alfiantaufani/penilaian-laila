<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-2 text-gray-800">Edit Data Indikator Penilaian</h1>
    <div class="row mb-5">
        <div class="col-md-12 mt-3">
            <div class="card">
                <div class="card-header">
                    Form Indikator Penilaian
                </div>
                <div class="card-body">
                    <form method="post" action="">
                        <input type="hidden" name="id" value="<?= $pernyataan->id; ?>" >
                        <div class="form-group">
                            <label for="komponen">Komponen</label>
                            <select name="komponen" class="form-control" id="komponen">
                                <option value="">-- Pilih --</option>
                                <?php foreach($komponen as $s) : ?>
                                    <?php if($pernyataan->id_komponen == $s->id) : ?>
                                        <option value="<?= $s->id ?>" selected><?= $s->nama ?></option>
                                    <?php else : ?>
                                        <option value="<?= $s->id ?>"><?= $s->nama ?></option>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                            </select>
                            <small class="form-text text-danger"><?= form_error('komponen') ?></small>
                        </div>
                        <div class="form-group">
                            <label for="pernyataan">Indikator</label>
                            <textarea name="pernyataan" id="pernyataan" class="form-control" autocomplete="off"><?= set_value('pernyataan') ? set_value('pernyataan') : $pernyataan->pernyataan; ?></textarea>
                            <?= form_error('pernyataan','<small class="text-danger">','</small>'); ?>
                        </div>
                        <button type="submit" name="simpan" class="btn btn-primary">Save</button>
                        <a href="<?= base_url('admin/Pernyataan') ?>" class="btn btn-secondary">Kembali</a>
                    </form>
                </div>
            </div>
        </div>
    </div>

</div>