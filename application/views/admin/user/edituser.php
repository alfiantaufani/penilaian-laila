<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-2 text-gray-800">Edit Data Admin</h1>

    <?php if($this->session->flashdata('flash')) : ?>
    <div class="row mt-3">
        <div class="col-md-12">
            <div class="alert alert-success alert-dismissible fade show" role="alert"><?= $this->session->flashdata('flash') ?>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        </div>
    </div>
    <?php endif; ?>
    <?php if($this->session->flashdata('gagal')) : ?>
    <div class="row mt-3">
        <div class="col-md-12">
            <div class="alert alert-danger alert-dismissible fade show" role="alert"><?= $this->session->flashdata('gagal') ?>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        </div>
    </div>
    <?php endif; ?>

    <div class="row mb-5">
        <div class="col-md-4">
            <form method="post" action="<?= base_url('admin/user/edituser') ?>">
                <div class="form-group">
                    <label for="nama">Nama</label>
                    <input type="text" class="form-control" id="nama" placeholder="Nama" name="nama" autocomplete="off" value="<?= set_value('nama') ? set_value('nama') : $nama->nama; ?>">
                    <?= form_error('nama','<small class="text-danger">','</small>'); ?>
                </div>
                <div class="form-group">
                    <label for="Username">Username</label>
                    <input type="text" class="form-control" placeholder="Username" name="user" autocomplete="off" value="<?= set_value('user') ? set_value('user') : $user->username; ?>">
                    <?= form_error('email','<small class="text-danger">','</small>'); ?>
                </div>
                <button type="submit" name="simpan" class="btn btn-primary">Simpan</button>
                <a href="<?= base_url('admin/user') ?>" class="btn btn-secondary">Kembali</a>
            </form>
        </div>
        <div class="col-md-4">
            <form method="post" action="<?= base_url('admin/user/editpass') ?>">
                <div class="form-group">
                    <label for="nama">Password Lama</label>
                    <input type="password" class="form-control" placeholder="******" name="pass_old" autocomplete="off" value="<?= set_value('pass_old')?>">
                    <?= form_error('pass_old','<small class="text-danger">','</small>'); ?>
                </div>
                <div class="form-group">
                    <label for="nama">Password Baru</label>
                    <input type="password" class="form-control" placeholder="******" name="pass_new" autocomplete="off" value="">
                    <?= form_error('pass_new','<small class="text-danger">','</small>'); ?>
                </div>
                <div class="form-group">
                    <label for="nama">Konfirmasi Password</label>
                    <input type="password" class="form-control" placeholder="******" name="konfir_pass" autocomplete="off" value="">
                    <?= form_error('konfir_pass','<small class="text-danger">','</small>'); ?>
                </div>
                <button type="submit" name="simpan" class="btn btn-primary">Ubah</button>
            </form>
        </div>
        <div class="col-md-4">
            <?php $userr = $this->db->get_Where('user',['id' => $this->session->userdata('id_user')])->row(); ?>
            <img src="<?= base_url('assets/img/'.$userr->gambar); ?>" class="card-img" alt=".....">

            <form action="<?= base_url('admin/user/simpanfoto') ?>" method="post" enctype="multipart/form-data">
                <div class="form-group">
                    <label for="nip">Upload Foto</label>
                    <input type="file" name="file" class="form-control" placeholder="Upload foto baru" required>
                </div>
                
                <button type="submit" class="btn btn-primary">Simpan foto</button>

            </form>
        </div>
    </div>

</div>