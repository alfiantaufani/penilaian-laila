<div class="container-fluid">
    <h1 class="h3 mb-2 text-gray-800">Data Nilai</h1>
    <div class="card shadow mb-4">
        <div class="card-body">
            <?php echo form_open_multipart('admin/pkg/cetak_pilihan');?>
            <div class="input-group mb-3">
              <select name="penilaian" class="form-control">
                <option disabled selected>--Pilih--</option>
                <option value="guru">Penilaian Guru</option>
                <option value="kepsek">Penilaian Kepsek</option>
            </select>
            <div class="input-group-append">
                <button  type="submit" name="simpan" class="btn btn-primary">Cetak Penilaian</button>
            </div>
            <?php echo form_close();?>
        </div>
    </div>
</div>
<div class="card shadow mb-4">
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Nama Penilai</th>
                        <th>Nama Dinilai</th>
                        <th>Jumlah Nilai</th>
                        <th>Rata Rata</th>
                        <th>Keterangan</th>
                        <th>Option</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $i=1; foreach($rekap_nilai as  $r) :  ?>
                    <tr>
                        <td><?= $i++; ?></td>
                            <?php if (empty($r->id_penilai)): ?>
                                <td>
                                    <?php echo $this->db->get_where('pegawai',['id' => $r->id_pegawai])->row()->nama; ?>
                                </td>
                            <?php else :?>
                                <td>
                                    <?php echo $this->db->get_where('guru',['id' => $r->id_penilai])->row()->nama; ?>
                                </td>
                                <?php endif; ?>
                        <td><?php echo $this->db->get_where('guru',['id' => $r->id_dinilai])->row()->nama; ?></td>
                                <?php
                                $this->db->select('sum(nilai) as ttl');
                                $this->db->where('id_penilai',$r->id_penilai);
                                $this->db->where('id_dinilai',$r->id_dinilai);
                                $ttl_nilai = $this->db->get('penilaian')->row(); ?>
                                <td><?= $ttl_nilai->ttl; ?></td>
                                <?php
                                $this->db->select('count(nilai) as ttl');
                                $this->db->where('id_penilai',$r->id_penilai);
                                $this->db->where('id_dinilai',$r->id_dinilai);
                                $jml_nilai = $this->db->get('penilaian')->row(); 
                                $total =$ttl_nilai->ttl / $jml_nilai->ttl;
                                if ($total > 70) {
                                    $kata ="Baik";
                                }else{
                                    $kata="Kurang Baik";
                                }
                                ?>
                                <td><?= $total; ?></td>
                                <td><?= $kata; ?></td>
                                <?php if (empty($r->id_penilai)): ?>
                                    <td>
                                        <a href="<?= base_url('admin/pkg/detailkepsek/'.$r->id_pegawai.'/'.$r->id_dinilai) ?>" class="badge badge-info float-right mr-1 mb-1">Detail</a>
                                        <a href="<?= base_url('admin/pkg/cetaklaporankepsek/'.$r->id_pegawai.'/'.$r->id_dinilai) ?>" class="badge badge-warning float-right mr-1" target="_blank">Cetak</a>
                                    </td>
                                <?php else :?>
                                    <td>
                                        <a href="<?= base_url('admin/pkg/detail/'.$r->id_penilai.'/'.$r->id_dinilai) ?>" class="badge badge-info float-right mr-1 mb-1">Detail</a>
                                        <a href="<?= base_url('admin/pkg/cetaklaporan/'.$r->id_penilai.'/'.$r->id_dinilai) ?>" class="badge badge-warning float-right mr-1" target="_blank">Cetak</a>
                                    </td>
                                <?php endif; ?>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<!-- <script type="text/javascript">
    var t =$('#dataTable-1').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            data: {
                id: "1"
            },
            url: "<?= base_url('admin/pkg/data_pkg') ?>"
        },
        {
            render:  function (data, type, row) {
                if ( row.id_penilai === null ) {
                    return 'Genre Utama Tidak Ada';
                }
                else {
                    return row.id_penilai;
                }
            }
                  
        },
        {
            data: 'id_dinilai',
            name: 'created_at'
        },
        {
            render:  function (data, type, row) {
                if ( row.id_pegawai === null ) {
                    return 'Genre Utama Tidak Ada';
                }
                else {
                    return row.id_pegawai;
                }
            }
        },
        {
            render: function (data, type, row){
                return '<button type="button" class="btn btn-warning btn-xs mx-2" onclick="Edit('+row.id+')" title="Edit Sub Genre" data-toggle="modal" data-target="#ModalEditSubGenre"><span class="material-icons-outlined">mode_edit</span></button>' +
                '<button class="btn btn-danger btn-xs" onclick="Delete('+ row.id +')" title="Delete Data"><span class="material-icons-outlined">delete_outline</span></button>'
            }
        }
        ],
        "lengthMenu": [10, 25, 50, 75, 100],
        "language": {
            "sEmptyTable": "Tidak ada data yang tersedia pada tabel ini",
            "sProcessing": "Sedang memproses...",
            "sLengthMenu": "Tampilkan _MENU_ entri",
            "sZeroRecords": "Tidak ditemukan data yang sesuai",
            "sInfo": "Menampilkan _START_ sampai _END_ dari _TOTAL_ entri",
            "sInfoEmpty": "Menampilkan 0 sampai 0 dari 0 entri",
            "sInfoFiltered": "(disaring dari _MAX_ entri keseluruhan)",
            "sInfoPostFix": "",
            "sSearch": "Cari:",
            "sUrl": "",
            "oPaginate": {
                "sFirst": "Pertama",
                "sPrevious": "Sebelumnya",
                "sNext": "Selanjutnya",
                "sLast": "Terakhir"
            }
        }
    })
</script> -->