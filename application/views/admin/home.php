<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800">Selamat Datang Di Aplikasi <br>PENILAIAN KINERJA GURU (PKG)</h1>

    <div class="row">

        <?php $guru = $this->db->get('guru')->num_rows(); ?>
        <div class="col-md-6">
            <div class="card border-left-success shadow h-100 py-2">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                <div class="col mr-2">
                    <div class="text-xs font-weight-bold text-success text-uppercase mb-1">Total Guru</div>
                    <div class="h5 mb-0 font-weight-bold text-gray-800"><?= $guru ?></div>
                </div>
                <div class="col-auto">
                    <i class="fas fa-users fa-2x text-gray-300"></i>
                </div>
                </div>
            </div>
            </div>
        </div>

        <?php 
        $this->db->group_by(array('id_penilai','id_dinilai','id_pegawai'));
        $Penilaian = $this->db->get('penilaian')->num_rows(); ?>
        <div class="col-md-6">
            <div class="card border-left-warning shadow h-100 py-2">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                <div class="col mr-2">
                    <div class="text-xs font-weight-bold text-warning text-uppercase mb-1"> Jumlah Penilaian</div>
                    <div class="h5 mb-0 font-weight-bold text-gray-800"><?= $Penilaian ?></div>
                </div>
                <div class="col-auto">
                    <i class="fas fa-users fa-2x text-gray-300"></i>
                </div>
                </div>
            </div>
            </div>
        </div>
    </div>

</div>
<!-- /.container-fluid -->