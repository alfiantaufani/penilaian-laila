<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Aplikasi PKG</title>

  <!-- Custom fonts for this template-->
  <link href="<?= base_url(); ?>/assets/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="<?= base_url(); ?>/assets/css/sb-admin-2.min.css" rel="stylesheet">

</head>

<body class="bg-gradient-primary">

  <div class="container">

    <!-- Outer Row -->
    <div class="row justify-content-center">

      <div class="col-md-7">

        <div class="card o-hidden border-0 shadow-lg my-5">
          <div class="card-body p-0">
            <!-- Nested Row within Card Body -->
            <div class="row">
              <div class="col-lg">
                <div class="p-5">
                  <div class="text-center">
                    <h1 class="h4 text-gray-900 mb-4">Registrasi User Baru</h1>
                  </div>
                  <form class="user" method="POST" action=""><!-- 
                    <div class="form-group">
                      <label for="nip">NIP</label>
                      <input type="text" class="form-control" id="nip" placeholder="NIP" name="nip" autocomplete="off" value="<?= set_value('nip'); ?>">
                      <?= form_error('nip','<small class="text-danger">','</small>'); ?>
                    </div> -->
                    <div class="form-group">
                      <label for="nama">Nama</label>
                      <input type="text" class="form-control" id="nama" placeholder="Nama" name="nama" autocomplete="off" value="<?= set_value('nama'); ?>">
                      <?= form_error('nama','<small class="text-danger">','</small>'); ?>
                    </div>
                    <div class="form-group">
                      <label for="nama">Nomor Seri Karpeg</label>
                      <input type="text" class="form-control" id="no_karpeg" placeholder="Nama" name="no_karpeg" autocomplete="off" value="<?= set_value('no_karpeg'); ?>">
                      <?= form_error('no_karpeg','<small class="text-danger">','</small>'); ?>
                    </div>
                    <div class="form-group">
                      <label for="jenis_kelamin">Jenis Kelamin</label>
                      <div class="form-check">
                        <input class="form-check-input" type="radio" name="jenis_kelamin" id="L" value="L" <?php if(set_value('jenis_kelamin') == 'L') echo "checked";  ?>>
                        <label class="form-check-label" for="L">
                          Laki-Laki
                        </label>
                        <input style="margin-left: 15px" class="form-check-input" type="radio" name="jenis_kelamin" id="P" value="P" <?php if(set_value('jenis_kelamin') == 'P') echo "checked";  ?>>
                        <label  style="margin-left: 30px" class="form-check-label" for="P">
                          Perempuan
                        </label>
                      </div>
                      <?= form_error('jenis_kelamin','<small class="text-danger">','</small>'); ?>
                    </div>
                    <div class="form-group">
                      <label for="tmp_lahir">Tempat Lahir</label>
                      <input type="text" class="form-control" id="tmp_lahir" placeholder="Tempat Lahir" name="tmp_lahir" autocomplete="off" value="<?= set_value('tmp_lahir'); ?>">
                      <?= form_error('tmp_lahir','<small class="text-danger">','</small>'); ?>
                    </div>
                    <div class="form-group">
                      <label for="tgl_lahir">Tanggal Lahir</label>
                      <input type="date" class="form-control" id="tgl_lahir" placeholder="Tempat Lahir" name="tgl_lahir" autocomplete="off" value="<?= set_value('tgl_lahir'); ?>">
                      <?= form_error('tgl_lahir','<small class="text-danger">','</small>'); ?>
                    </div>
                    <div class="form-group">
                      <label for="nama">Pendidikan Terakhir</label>
                      <input type="text" class="form-control" id="pendidikan_terakhir" placeholder="Pendidikan Terakhir" name="pendidikan_terakhir" autocomplete="off" value="<?= set_value('pendidikan_terakhir'); ?>">
                      <?= form_error('pendidikan_terakhir','<small class="text-danger">','</small>'); ?>
                    </div>
                    <div class="form-group">
                      <label for="nama">Pangkat / Golongan</label>
                      <input type="text" class="form-control" id="pangkat" placeholder="Pangkat / Golongan" name="pangkat" autocomplete="off" value="<?= set_value('pangkat'); ?>">
                      <?= form_error('pangkat','<small class="text-danger">','</small>'); ?>
                    </div>
                    <div class="form-group">
                      <label for="nama">NRG</label>
                      <input type="text" class="form-control" id="nrg" placeholder="NRG" name="nrg" autocomplete="off" value="<?= set_value('nrg'); ?>">
                      <?= form_error('nrg','<small class="text-danger">','</small>'); ?>
                    </div>
                    <div class="form-group">
                      <label for="nama">NUPTK</label>
                      <input type="text" class="form-control" id="nuptk" placeholder="NUPTK" name="nuptk" autocomplete="off" value="<?= set_value('nuptk'); ?>">
                      <?= form_error('nuptk','<small class="text-danger">','</small>'); ?>
                    </div>
                    <div class="form-group">
                      <label for="username">Username</label>
                      <input type="text" class="form-control" id="username" placeholder="Username" name="username" autocomplete="off" value="<?= set_value('username'); ?>">
                      <?= form_error('username','<small class="text-danger">','</small>'); ?>
                    </div>
                    <div class="form-group row">
                      <div class="col-sm-6 mb-3 mb-sm-0">
                        <label for="password1">Password</label>
                        <input type="password" class="form-control" id="password1" placeholder="Password" name="password1" autocomplete="off">
                        <?= form_error('password1','<small class="text-danger">','</small>'); ?>
                      </div>
                      <div class="col-sm-6">
                        <label for="password2">Ulangi Password</label>
                        <input type="password" class="form-control" id="password2" placeholder="Ulangi Password" name="password2" autocomplete="off">
                      </div>
                    </div>
                    <button type="submit" class="btn btn-primary btn-user btn-block">
                      Registrasi
                    </button>
                  </form>
                  <hr>
                  <div class="text-center">
                    <a class="small" href="<?= base_url('auth'); ?>">Sudah Punya Akun? Login!</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

      </div>

    </div>

  </div>

  <!-- Bootstrap core JavaScript-->
  <script src="<?= base_url(); ?>/assets/vendor/jquery/jquery.min.js"></script>
  <script src="<?= base_url(); ?>/assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="<?= base_url(); ?>/assets/vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="<?= base_url(); ?>/assets/js/sb-admin-2.min.js"></script>

</body>

</html>
