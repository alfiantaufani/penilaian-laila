<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Laporan Hasil Penilaian Kinerja Guru</title>

    <style>
     * {
         box-sizing: border-box;
         font-family: times-new-roman;
     }
     body {
       margin: 0;
       font-family: times-new-roman;
       font-size: 14px;
   }

   /*design table 1*/
    /* .apik {
        font-family: sans-serif;
        color: #232323;
        border-collapse: collapse;
    }
    
    .apik, th, td {
        border: 1px solid #999;
        padding: 8px 20px;
        } */
    </style>

</head>
<body>

    <table>
        <tr>
            <td valign="top">
                <img src="<?= 'assets/img/logo_kementrian.png'?>" alt="" width="17%">
            </td>
            <td>
                <div style="text-align:center; margin-left:20px">
                    <h2 style="margin-top:0px; margin-botom:0px;">PEMERINTAH KABUPATEN JOMBANG</h2>
                    <h1 style="margin-top:-20px; margin-botom:0px;">DINAS PENDIDIKAN</h1>
                    <p align="center" style="margin-top:-20px;font-size:16px">
                        Jl. Patimura No. 5 ,Sengon Kab Jombang Telp. (0321) 86827  <br>
                        website : cabdindikjombang.com email : disdik.jombang@yahoo.com
                    </p>
                    <h2 style="margin-top:-15px; margin-botom:0px;">JOMBANG - 61418</h2>
                </div>
            </td>
        </tr>
    </table>
    
    <hr style="margin-top:-20px; weight:600">
    <br>

    <table>
        <tr>
            <td valign="top">Nomor</td>
            <td valign="top">:</td>
            <td valign="top">143/B3/2019</td>
        </tr>
        <tr>
            <td valign="top">Hal</td>
            <td valign="top">:</td>
            <td valign="top">Hasil penilaian PKG <br> a.n <?= $dinilaian->nama ?></td>
        </tr>
    </table>


    <p>Yth. Kepala Sekolah <br>
        <?= $kepsek->nama ?> <br>
    Kab. Jombang Prov. Jawa Timur Kode Pos 61418</p>
    <table style="color: #232323; border-collapse: collapse; width:100%">
        <tr style="border: 1px solid #999;">
            <td style="border: 1px solid #999; padding:4px;font-size:13px"><b>No</b></td>
            <td colspan="2" style="border: 1px solid #999; padding:4px;font-size:13px"><b>Indikator</b></td>
            <td style="width: 150px; border: 1px solid #999; padding:4px;font-size:13px"><b>Nilai</b></td>
        </tr style="border: 1px solid #999;">
        <?php $i = 1; foreach($kegiatan as $k): ?>
        <tr style="border: 1px solid #999;">
            <td style="width: 10px;border: 1px solid #999;padding-left:3px;text-align:center;font-family: sans-serif;font-size:13px">
                <?= $i++; ?>
            </td>
            <td style="border: 1px solid #999;padding-left:3px;font-family: times-new-roman;font-size:13px"colspan="3"><?= $k->nama; ?>
            </td>
        </tr>
            <?php
            $datapernyataan = $this->db->get_where('pernyataan', ['id_komponen' => $k->id])->result();
            foreach($datapernyataan as $d): ?>
        <tr style="border: 1px solid #999;">
            <td style="border: 1px solid #999;padding-left:3px;text-align:center;font-family: sans-serif;font-size:13px">
            </td>
            <td style="width: 10px;border: 1px solid #999;padding-left:3px;font-family: times-new-roman;font-size:13px">
                *
            </td>
            <td style="border: 1px solid #999;padding-left:3px;font-family: times-new-roman;font-size:13px"><?= $d->pernyataan ?>
            </td>
            <td style="border: 1px solid #999;padding-left:3px;font-family: times-new-roman;font-size:13px">
                <?php
                    $this->db->where('id_dinilai',$dinilai);
                    if ($penilai== "") {
                        $this->db->where('id_pegawai',$penilai1);
                    }else{
                        $this->db->where('id_penilai',$penilai);
                    }
                    $this->db->where('id_pernyataan',$d->id);
                    $nilai = $this->db->get('penilaian')->row(); ?>
                    <?= $nilai->nilai ?>
            </td>
        </tr>
        <?php endforeach; ?>
        <?php endforeach; ?>
        <tr style="border: 1px solid #999;">
            <td style="border: 1px solid #999;padding-left:3px;text-align:center;font-family: sans-serif;font-size:13px">

            </td>
            <td colspan="2" style="border: 1px solid #999;padding-left:3px;font-family: times-new-roman;font-size:13px">
                <b>Jumlah Unsur Utama Dan Unsur Penunjang</b>
            </td>
            <td style="border: 1px solid #999;padding-left:3px;font-family: times-new-roman;font-size:13px">

            </td>
        </tr>
    </table>

    <span style="font-size:10px;">*) Penyesuaian PAK atau PAK terakhir</span> <br>
    <span style="font-size:10px;">**) Angka kredit yang diperoleh</span>

</body>
</html>