<nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">
    <!-- Sidebar Toggle (Topbar) -->
    <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
    <i class="fa fa-bars"></i>
    </button>

    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"> <i class="fas fa-fw fa-home"></i> <a href="<?= base_url('admin/home') ?>">PKG</a></li>
            <li class="breadcrumb-item active" aria-current="page"><?= ucfirst($this->uri->segment(2, null)); ?></li>
        </ol>
    </nav>

    <!-- Topbar Navbar -->
    <ul class="navbar-nav ml-auto">
        <div class="topbar-divider d-none d-sm-block"></div>

        <?php $datagambar = $this->db->get_where('user', ['id' => $this->session->userdata('id_user')])->row() ?>

        <!-- Nav Item - User Information -->
        <li class="nav-item dropdown no-arrow">
            <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <span class="mr-2 d-none d-lg-inline text-gray-600 small"><?= $this->session->userdata('nama') ?></span>
            <img class="img-profile rounded-circle" src="<?= base_url('assets/img/'.$datagambar->gambar); ?>">
            </a>
            <!-- Dropdown - User Information -->
            <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
            <?php if($this->session->userdata('role_id') == 1) : ?>
            <a class="dropdown-item" href="<?= base_url('admin/user') ?>">
                <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                Profil
            </a>
            <?php endif; ?>
            <?php if($this->session->userdata('role_id') == 2) : ?>
            <a class="dropdown-item" href="<?= base_url('guru/user') ?>">
                <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                Profil
            </a>
            <?php endif; ?>
            <?php if($this->session->userdata('role_id') == 3) : ?>
            <a class="dropdown-item" href="<?= base_url('kepsek/user') ?>">
                <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                Profil
            </a>
            <?php endif; ?>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">
                <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                Logout
            </a>
            </div>
        </li>

    </ul>

</nav>