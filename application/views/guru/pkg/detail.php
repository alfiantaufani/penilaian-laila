

<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800">Detail Penilaian</h1>

    <?php if($this->session->flashdata('flash')) : ?>
        <div class="row mt-3">
            <div class="col-md-12">
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    <strong><?= $this->session->flashdata('flash') ?></strong>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </div>
        </div>
    <?php endif; ?>
    
    <table class="table table-hover table-bordered">
        <thead>
            <tr>
                <td width="10px">No</td>
                <td colspan="2">Indikator</td>
                <td width="170px">Nilai</td>
            </tr>
        </thead>
        <tbody>
            <?php $i=1; foreach($kegiatan as $k): ?>
            <tr>
                <td><?= $i++; ?></td>
                <td colspan="3"><?= $k->nama; ?></td>
            </tr>
            <?php $datapernyataan = $this->db->get_where('pernyataan', ['id_komponen' => $k->id])->result();
            foreach($datapernyataan as $d): ?>
                <tr>
                    <td></td>
                    <td width="10px">A</td>
                    <td>
                        <div>
                            <label class="form-check-label" for="<?= $d->pernyataan ?>">
                                <?= $d->pernyataan ?>
                            </label>
                        </div>
                    </td>
                    <td>
                        <?php
                        $this->db->where('id_penilai',$this->session->userdata('id'));
                        $this->db->where('id_dinilai',$id);
                        $this->db->where('id_pernyataan',$d->id);
                        $nilai = $this->db->get('penilaian')->row(); ?>
                        <div class="form-check">
                            <label class="form-check-label">
                                <?= $nilai->nilai ?>
                            </label>
                        </div>
                    </td>
                </tr>
            <?php endforeach; ?>
        <?php endforeach; ?>
    </tbody>
</table>

<a href="<?= base_url('guru/pkg') ?>" class="btn btn-secondary">Kembali</a>

<br><br>


</div>
<!-- /.container-fluid -->