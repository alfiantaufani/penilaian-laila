<div class="container-fluid">

    <h1 class="h3 mb-2 text-gray-800">Data Nilai</h1>

    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <a href="<?= base_url('guru/pkg/penilaian'); ?>" class="btn btn-primary"> Tambah Penilaian </a>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama Dinilai</th>
                            <th>Tanggal Nilai</th>
                            <th>Jumlah Nilai</th>
                            <th>Rata Rata</th>
                            <th>Option</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $i=1; foreach($rekap_nilai as  $r) :  ?>
                        <tr>
                            <td><?= $i++; ?></td>
                            <td><?= $r['nama']; ?></td>
                            <td><?= date('d F Y',$r['tgl']) ?></td>
                            <?php
                            $aaa =$r['id_dinilai'];
                            $this->db->select('sum(nilai) as ttl');
                            $this->db->where('id_dinilai',$aaa);
                            $this->db->where('id_penilai',$this->session->userdata('id'));
                            $ttl_nilai = $this->db->get('penilaian')->row(); ?>
                            <td><?= $ttl_nilai->ttl; ?></td>
                            <?php
                            $this->db->select('count(nilai) as ttl');
                            $this->db->where('id_dinilai',$aaa);
                            $this->db->where('id_penilai',$this->session->userdata('id'));
                            $jml_nilai = $this->db->get('penilaian')->row(); 
                            $total =$ttl_nilai->ttl / $jml_nilai->ttl;?>
                            <td><?= $total; ?></td>
                            <td>
                                <a href="<?= base_url('guru/pkg/detail/'.$r['id_dinilai']) ?>" class="badge badge-info float-right mr-1 mb-1">Detail</a>
                                <!-- <a href="<?= base_url('guru/pkg/cetaklaporan/'.$r['id_dinilai']) ?>" class="badge badge-warning float-right mr-1" target="_blank">Cetak</a> -->
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>

</div>