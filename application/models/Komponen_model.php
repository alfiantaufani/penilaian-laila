<?php

class komponen_model extends CI_model {

    public function countAll()
    {
        $query = $this->db->get("komponen");
        return $query->num_rows();
    }

    public function getAll()
    {
        return $this->db->get("komponen")->result();
    }

    public function get($id)
    {
        return $this->db->get_where("komponen", ['id' => $id])->row();
    }
    public function insert()
    {
        $data = [
            "nama" => $this->input->post('komponen', true),
            "type" => $this->input->post('type', true),
        ];
        $this->db->insert('komponen', $data);
    }

    public function update()
    {
        $data = [
            "nama" => $this->input->post('komponen', true),
            "type" => $this->input->post('type', true),
        ];
        $this->db->where('id', $this->input->post('id', true));
        $this->db->update('komponen', $data);
    }

    public function delete($id)
    {
        $this->db->delete('komponen', ['id' => $id]);
    }
}