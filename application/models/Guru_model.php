<?php

class Guru_model extends CI_model {

    public function countAll()
    {
        $query = $this->db->get("guru");
        return $query->num_rows();
    }

    public function getAll()
    {
        return $this->db->get('guru')->result();
    }

    public function get($id)
    {
        return $this->db->get_where('guru', ['id' => $id])->row();
    }

    public function insert()
    {
        $data = [
            "nip"           => $this->input->post('nip'),
            "nama"          => $this->input->post('nama', true),
            "no_karpeg"     => $this->input->post('no_karpeg'),
            "jenis_kelamin" => $this->input->post('jenis_kelamin', true),
            "tmp_lahir"     => $this->input->post('tmp_lahir', true),
            "tgl_lahir"     => $this->input->post('tgl_lahir', true),
            "pendidikan_terakhir" => $this->input->post('pendidikan_terakhir'),
            "pangkat"       => $this->input->post('pangkat'),
            "nrg"           => $this->input->post('nrg'),
            "nuptk"         => $this->input->post('nuptk'),
            "mapel"         => $this->input->post('mapel'),
            "jam_mengajar"  => $this->input->post('jam_mengajar'),
        ];
        $this->db->insert('guru', $data);
        $data2 = [
            "id_guru" => $this->db->insert_id(),
            "username" => $this->input->post('username', true),
            "password" => md5($this->input->post('password')),
            "gambar" => "avatar.jpg",
            "role_id" => 2,
            "is_active" => 1,
            "date_created" => time(),
        ];
        $this->db->insert('user', $data2);
    }

    public function update()
    {
        $data = [
            "nip"           => $this->input->post('nip'),
            "nama"          => $this->input->post('nama', true),
            "no_karpeg"     => $this->input->post('no_karpeg'),
            "jenis_kelamin" => $this->input->post('jenis_kelamin', true),
            "tmp_lahir"     => $this->input->post('tmp_lahir', true),
            "tgl_lahir"     => $this->input->post('tgl_lahir', true),
            "pendidikan_terakhir" => $this->input->post('pendidikan_terakhir'),
            "pangkat"       => $this->input->post('pangkat'),
            "nrg"           => $this->input->post('nrg'),
            "nuptk"         => $this->input->post('nuptk'),
            "mapel"         => $this->input->post('mapel'),
            "jam_mengajar"  => $this->input->post('jam_mengajar'),
        ];
        $this->db->where('id', $this->input->post('id', true));
        $this->db->update('guru', $data);
    }

    public function delete($id)
    {
        $this->db->delete('guru', ['id' => $id]);
        $this->db->delete('user', ['id_guru' => $id]);
    }
}