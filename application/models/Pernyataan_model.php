<?php

class Pernyataan_model extends CI_model {

    public function countAll()
    {
        $query = $this->db->get("pernyataan");
        return $query->num_rows();
    }

    public function getAll()
    {
        $this->db->select("pernyataan.*,komponen.nama");
        $this->db->from('pernyataan');
        $this->db->join('komponen','pernyataan.id_komponen=komponen.id');
        return $this->db->get()->result();
    }

    public function get($id)
    {
        $this->db->select("pernyataan.*,komponen.nama");
        $this->db->from('pernyataan');
        $this->db->join('komponen','pernyataan.id_komponen=komponen.id');
        $this->db->where('pernyataan.id',$id);
        return $this->db->get()->row();
    }

    public function komponen()
    {
        return $this->db->get('komponen')->result();
    }

    public function insert()
    {
        $data = [
            "id_komponen" => $this->input->post('komponen', true),
            "pernyataan" => $this->input->post('pernyataan', true),
        ];
        $this->db->insert('pernyataan', $data);
    }

    public function update()
    {
        $data = [
            "id_komponen" => $this->input->post('komponen', true),
            "pernyataan" => $this->input->post('pernyataan', true),
        ];
        $this->db->where('id', $this->input->post('id', true));
        $this->db->update('pernyataan', $data);
    }

    public function delete($id)
    {
        $this->db->delete('pernyataan', ['id' => $id]);
    }
}